from django.contrib.auth.models import User, Group
from .models import EXTLynxDB
from urllib.parse import urlparse
from django.shortcuts import redirect
from django.contrib import auth
from django.template.context_processors import csrf
from django.views import View
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login
from django.shortcuts import render
from .forms import UserForm
from django.contrib import messages
from django.utils.http import is_safe_url, urlunquote
from django.contrib.auth import logout
from blog_mks.sql import csql

import pyodbc


class LoginView(View):
    # To Do
    # сделать логи входа

    def get(self, request):
        if auth.get_user(request).is_authenticated:
            return redirect('handbook:post_list')
        else:
            userform = UserForm()
            return render(request, 'accounts/login.html', {"form": userform})

    def post(self, request):
        # проверяем форму и данные
        userform = UserForm(request.POST)
        if userform.is_valid():
            username = userform.cleaned_data["username"]
            password = userform.cleaned_data["password"]
            user = self.authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                next = urlparse(get_next_url(request)).path
                if next == '/admin/login/' and request.user.is_staff:
                    return redirect('/admin/')
                return redirect(next)
            else:
                messages.error(request, 'Неправильный логин или пароль')
                return render(request, 'accounts/login.html', {'form': userform})
        else:
            return render(request, 'accounts/login.html', {'form': userform})

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def authenticate(self, username=None, password=None):
        try:
            username = username.lower()
            cursor = csql.sqlconnect(database='')
            cursor.execute("""select 
                             ViewUsers.user_id,
                             ViewUsers.user_name,
                             ViewEmploys.Family,
                             ViewEmploys.FirstName,
                             ViewEmploys.SecondName,
                             ViewEmploys.Domain_ID,
                             ViewUsers.email, 
                             ViewUsers.Employ_id,
                             ViewUsers.Visor, 
                             ViewEmploys.Job_Name,
                             ViewEmploys.Domain_Name
                            from ViewUsers, ViewEmploys
                            where ViewUsers.user_type = 1
                            and ViewUsers.Employ_id = ViewEmploys.Employ_ID 
                            and ViewUsers.user_name = '%s'""" % username)

            for row in cursor.fetchall():
                ldb_domain_id = row.Domain_ID
                ldb_user_id = row.user_id
                first_name = row.FirstName
                ldb_secondname = row.SecondName
                family = row.Family
                email = row.email
                ldb_employid = row.Employ_id
                ldb_visor = row.Visor
                ldb_jobname = row.Job_Name
                ldb_domainname = row.Domain_Name

        except pyodbc.Error as ex:
            sqlstate = ex.args[1]
            # сделать логи входа
            print(sqlstate)
            return None

        try:
            user = User.objects.get(username=username)
            if user:
                user = User.objects.get(username=username)
                user.first_name = first_name
                user.last_name = family
                user.email = email
                user.set_password(password)
                user.save()

                ext_user = EXTLynxDB.objects.get(user=user)
                ext_user.ldb_domain_id = ldb_domain_id
                ext_user.ldb_user_id = ldb_user_id
                ext_user.ldb_employid = ldb_employid
                ext_user.ldb_visor = ldb_visor
                ext_user.ldb_jobname = "%s" % ldb_jobname
                ext_user.ldb_secondname = "%s" % ldb_secondname
                ext_user.ldb_domainname = "%s" % ldb_domainname
                ext_user.save()

                g = Group.objects.get(name='%s' % ldb_jobname)
                g.user_set.add(user)

        except User.DoesNotExist:
            try:
                user = User.objects.create(username=username, first_name=first_name,
                                           last_name=family, email=email)
                ext_user = EXTLynxDB.objects.create(ldb_domain_id=ldb_domain_id, ldb_user_id=ldb_user_id,
                                                    ldb_employid=ldb_employid, ldb_visor=ldb_visor,
                                                    ldb_secondname=ldb_secondname, ldb_jobname=ldb_jobname,
                                                    ldb_domainname=ldb_domainname, user=user)
                user.set_password(password)
                user.save()
                ext_user.user.save()

                g = Group.objects.get(name='Служебный_Инструкции')
                g2 = Group.objects.get(name='Служебный_Все')
                g3 = Group.objects.get(name='%s' % ldb_jobname)
                g.user_set.add(user)
                g2.user_set.add(user)
                g3.user_set.add(user)
            except:
                return None
        return user


def logout_view(request):
    logout(request)
    return redirect('main:landing_page')


def create_context_username_csrf(request):
    context = {}
    context.update(csrf(request))
    context['login_form'] = AuthenticationForm
    return context


def get_next_url(request):
    next = request.META.get('HTTP_REFERER')
    if next:
        next = urlunquote(next)  # HTTP_REFERER may be encoded.
    if not is_safe_url(url=next, allowed_hosts=request.get_host()):
        next = '/'
    return next
