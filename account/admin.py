from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import EXTLynxDB


# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class EXTLynxDBInline(admin.StackedInline):
    model = EXTLynxDB
    can_delete = False
    verbose_name_plural = 'Внешние данные с Lynx_DB'


# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (EXTLynxDBInline,)


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
