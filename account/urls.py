from django.conf.urls import include, url
from . import views
from mks_data import registrations
from django.conf import settings
from django.urls import path

from . import groups_update

app_name = 'account'

urlpatterns = [
    path(r'login/', views.LoginView.as_view(), name='login'),
    path(r'logout/', views.logout_view, name='logout'),
    path(r'create_groups/', groups_update.create_groups),
]

