from django.db import models
from django.contrib.auth.models import User


class EXTLynxDB(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    ldb_domain_id = models.IntegerField()
    ldb_user_id = models.IntegerField()
    ldb_employid = models.IntegerField()
    ldb_visor = models.IntegerField()
    ldb_secondname = models.CharField(max_length=20)
    ldb_jobname = models.CharField(max_length=50, null=True)
    ldb_domainname = models.CharField(max_length=50, null=True)
