from django import forms


class UserForm(forms.Form):
    username = forms.SlugField(required=True,
                           widget=forms.TextInput(attrs={"class": "form-control form-control-user",
                                                         'placeholder': 'Логин'}))
    password = forms.SlugField(widget=forms.PasswordInput(attrs={"class": "form-control form-control-user",
                                                                 'placeholder': 'Пароль'}),
                               required=True)
    field_order = ["username", "password"]
    required_css_class = "form-control form-control-user"
    error_css_class = "error"