from django.contrib.auth.models import Group
from django.shortcuts import render, redirect

from blog_mks.sql import csql


def create_groups(request):
    try:
        cursor = csql.sqlconnect(database='')
        cursor.execute("""SELECT [Job_Name]
                          FROM [ViewUsersList]""")
        for row in cursor.fetchall():
            if row.Job_Name:
                Group.objects.get_or_create(name="%s" % row.Job_Name)
    except:
        pass
    return redirect('main:landing_page')
