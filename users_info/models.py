from django.db import models
from django.contrib.auth.models import Group


class UsersExtLynxEmploys(models.Model):
    employ_id = models.IntegerField(verbose_name='ИД Employs', unique=True)
    name = models.CharField(max_length=70, verbose_name='Пользователь')
    active = models.BooleanField(verbose_name='Работает\\Уволен')
    family = models.CharField(max_length=50, verbose_name='Фамилия')
    first_name = models.CharField(max_length=50, verbose_name='Имя')
    second_name = models.CharField(max_length=50, verbose_name='Отчество')
    department_name = models.CharField(max_length=50, verbose_name='Подразделение', blank=True)
    domain_id = models.IntegerField(verbose_name='ID подразделение', blank=True, null=True)
    domain_name = models.CharField(max_length=50, verbose_name='Территория', blank=True)
    job_name = models.CharField(max_length=50, verbose_name='Должность', blank=True)
    date_of_end = models.DateField(verbose_name='Дата увольнения', blank=True, null=True)
    birth_date = models.DateField(verbose_name='Дата рождения', blank=True, null=True)
    vocation_date_of_begin = models.DateField(verbose_name='Отпуск с', blank=True, null=True)
    vocation_date_of_end = models.DateField(verbose_name='Отпуск до', blank=True, null=True)

    class Meta:
        verbose_name = 'пользователя Lynx Employs'
        verbose_name_plural = 'Пользователи Lynx Employs'

    def __str__(self):
        return "{0} [{1}]".format(self.name, self.employ_id)


class UsersExtAD(models.Model):
    object_sid = models.CharField(max_length=255, verbose_name='ИД AD', unique=True)
    display_name = models.CharField(max_length=50, verbose_name='Пользователь', blank=True)
    user_name = models.CharField(max_length=50, verbose_name='Логин', blank=True)
    phone_local = models.CharField(max_length=50, verbose_name='Внутренний номер', blank=True)
    phone_mobile = models.CharField(max_length=50, verbose_name='Мобильный номер', blank=True)
    email = models.CharField(max_length=70, verbose_name='Почтовый адрес', blank=True)
    is_disabled = models.BooleanField(verbose_name='Активен')
    employ_id = models.ForeignKey(UsersExtLynxEmploys, verbose_name='ИД пользователя Lynx_Employs',
                                  on_delete=models.CASCADE, related_name='usersextad_set')

    class Meta:
        verbose_name = 'пользователя AD'
        verbose_name_plural = 'Пользователи AD'

    def __str__(self):
        return "{0} [{1}]".format(self.display_name, self.user_name)


class UsersExtLynxUsers(models.Model):
    user_id = models.IntegerField(verbose_name='ИД LynxUser')
    user_type = models.CharField(max_length=50, verbose_name='Тип')
    visor = models.CharField(max_length=50, verbose_name='Визор')
    user_name = models.CharField(max_length=50, verbose_name='Логин пользователя')
    domain_name = models.CharField(max_length=50, verbose_name='Склад', blank=True)
    is_disabled = models.BooleanField(verbose_name='Активен')
    adm_user = models.BooleanField(verbose_name='Администратор')
    employ_id = models.OneToOneField(UsersExtLynxEmploys, verbose_name='ИД LynxEmploys', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'пользователя Lynx Users'
        verbose_name_plural = 'Пользователи Lynx Users'

    def __str__(self):
        return "{0} [{1}]".format(self.user_name, self.employ_id)


class UsersExtTablet(models.Model):
    employ_id = models.ForeignKey(UsersExtLynxEmploys, verbose_name='ИД пользователя Lynx_Employs',
                                  on_delete=models.CASCADE, related_name='usersexttablet_set')
    sim = models.CharField(max_length=50, verbose_name='Номер сим-карты')
    comment = models.TextField(max_length=50, verbose_name='Комментарий', blank=True)
    name_ter = models.CharField(max_length=50, verbose_name='Наименование территории', blank=True)
    full_ter = models.CharField(max_length=50, verbose_name='Описание территории', blank=True)
    count = models.CharField(max_length=50, verbose_name='Численность', blank=True)

    class Meta:
        verbose_name = 'информация о планшетах'
        verbose_name_plural = 'Информация о планшетах'

    def __str__(self):
        return "{}".format(self.employ_id)


class UsersExtSIP(models.Model):
    employ_id = models.ForeignKey(UsersExtLynxEmploys, verbose_name='ИД пользователя Lynx_Employs',
                                  on_delete=models.CASCADE, related_name='usersextsip_set')
    speed_dial = models.CharField(max_length=50, verbose_name='Быстрый набор', blank=True)
    display_name = models.CharField(max_length=50, verbose_name='Отображаемое имя', blank=True)
    number = models.CharField(max_length=50, verbose_name='Внешний номер', blank=True, unique=True)

    class Meta:
        verbose_name = 'пользователя SIP'
        verbose_name_plural = 'Пользователи SIP'

    def __str__(self):
        return self.display_name


class UsersExtDBOS(models.Model):
    employ_id = models.ForeignKey(UsersExtLynxEmploys, verbose_name='ИД пользователя Lynx_Employs',
                                  on_delete=models.CASCADE, related_name='usersrxtdbos_set')
    card_id = models.CharField(max_length=250, verbose_name='ИД карточки')
    invent_id = models.CharField(max_length=250, verbose_name='Инвентарный номер', unique=True)
    name = models.CharField(max_length=250, verbose_name='Название')
    name_podr = models.CharField(max_length=250, verbose_name='Название подразделения')
    name_mol = models.CharField(max_length=250, verbose_name='МОЛ')
    name_ob = models.CharField(max_length=250, verbose_name='Относится')
    cena_new = models.CharField(max_length=250, verbose_name='Цена')
    dps_data = models.DateTimeField(verbose_name='Дата перещения')

    class Meta:
        verbose_name = 'основные средства'
        verbose_name_plural = 'Основные средства'
        ordering = ('invent_id',)

    def __str__(self):
        return self.name


class General(models.Model):
    general_groups_admin = models.ForeignKey(Group, related_name='users_info_groups_admin',
                                             verbose_name='Администратор группы',
                                             on_delete=models.CASCADE, null=True)

    general_groups = models.ForeignKey(Group, related_name='users_info_general_groups',
                                       verbose_name='Разрешение для группы',
                                       on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = 'Группа доступа'
        verbose_name_plural = 'группа доступа'

    def __str__(self):
        return str(self.general_groups)


class BlankAccess(models.Model):
    employ_id = models.ForeignKey(UsersExtLynxEmploys, verbose_name='ИД пользователя Lynx_Employs',
                                  on_delete=models.CASCADE, related_name='usersextblankaccess_set')
