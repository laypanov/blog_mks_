import six
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import user_passes_test
from .models import General
from django.contrib.auth.models import User


def group_required(view=None, group=None, login_url=None, raise_exception=False):
    """
    Декоратор для вью. Разрешает доступ в случае нахождения пользователя
    в одной из указанных групп.
    """

    def check_perms(user):
        try:
            group = General.objects.get(id=1)
            # общая группа доступа
            general_groups = group.general_groups
            if isinstance(general_groups, six.string_types):
                groups = (general_groups,)
            else:
                groups = general_groups
        except:
            return False

        if user.is_authenticated:
            if user.groups.filter(name=groups).exists():
                return True
        else:
            if str(general_groups) == 'Служебный_Все':
                return True

        if raise_exception:
            raise PermissionDenied
        return False

    # Возвращаем окончательный вердикт декоратора касаемо
    # проверки запрошенных прав.

    return user_passes_test(check_perms)


def check_perms_admin(user):
    if user.is_authenticated:
        try:
            group = General.objects.get(id=1)
            # общая группа доступа
            general_groups = group.general_groups_admin
            if isinstance(general_groups, six.string_types):
                groups = (general_groups,)
            else:
                groups = general_groups
        except:
            return False

        if user.groups.filter(name=groups).exists():
            return True
    return False
