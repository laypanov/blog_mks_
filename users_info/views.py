import json
import re
import urllib.request

import pyodbc
import requests
from django.contrib.auth.models import User
from django.shortcuts import render
from ldap3 import Server, Connection, SUBTREE, ALL, MODIFY_REPLACE
from django.utils.timezone import now
import dateutil.parser

from account.models import EXTLynxDB
from .conn_pass import *
from .models import UsersExtAD, UsersExtLynxEmploys, UsersExtLynxUsers, UsersExtDBOS, UsersExtSIP, UsersExtTablet
from .utils import group_required


@group_required()
def view_tablets(request):
    ext_department_name = 0

    ldb_username = request.user.username

    view_full_os = False

    try:
        user = User.objects.get(username=ldb_username)
        ext_user = EXTLynxDB.objects.get(user=user)
        ldb_employid = ext_user.ldb_employid
    except:
        ldb_employid = 0

    # Директора РТП видят весь свой офис
    if ldb_username == "dirkrd":
        ext_department_name = "РТП Краснодар"
    if ldb_username == "dirvlg":
        ext_department_name = "РТП Волгоград"
    if ldb_username == "dircrimea" or ldb_username == "alexeysimf":
        ext_department_name = "РТП Крым"
    if ldb_username == "dirsea":
        ext_department_name = "РТП Новороссийск"
    if ldb_username == "dirkmv":
        ext_department_name = "РТП Пятигорск"
    if ldb_username == "dirstv":
        ext_department_name = "РТП Ставрополь"
    if ldb_username == "dircaspiy":
        ext_department_name = "РТП Махачкала"

    ol = UsersExtLynxEmploys.objects.filter(active=1).exclude(department_name='Декрет') \
        .order_by('-domain_name', 'department_name', 'job_name', 'employ_id')

    return render(request, 'users_info/list_users.html', {
        'object_list': ol,
        'view_full_os': view_full_os,
        'ext_department_name': ext_department_name,
        'ldb_employid': ldb_employid,
        'today': now().date()
    })


def sync_db(request):
    # связываем AD и Employ_ID(Lynx)
    username = lynx_user_admin
    password = lynx_password_admin

    ad_server = ''
    ad_user = ad_user_admin
    ad_password = ad_password_admin
    ad_search_tree = ''

    server = Server(ad_server, get_info=ALL)
    conn = Connection(server, user=ad_user, password=ad_password)
    conn.bind()

    with urllib.request.urlopen("http://url/") as url:
        data = json.loads(url.read().decode())

    url = 'http://url/'
    headers = {'Content-type': 'application/json',
               'Accept': 'text/plain',
               'Content-Encoding': 'utf-8'}

    cursor = cnxn.cursor()

    cursor.execute("""
                    SELECT 
                           [Employ_ID]
                          ,[active]
                          ,[name]
                          ,[Family]
                          ,[FirstName]
                          ,[SecondName]
                          ,[Department_Name]
                          ,[Domain_Name]
                          ,[Job_Name]
                      FROM [ViewEmploys]
                      where active = 0""")

    # возможно понадобится удалить из списка отключенных
    for row in cursor.fetchall():
        for d in data:
            d = d.split(",")
            display_name_sip = d[2].strip().split("[")
            employ_id_sip = display_name_sip[1].split("]")
            employ_id_sip = employ_id_sip[0]

        if row.Employ_ID == employ_id_sip:
            print("ОТКЛЮЧЕН #нужно доделать удаление", row)

    cursor.execute("""
                    SELECT 
                           [Employ_ID]
                          ,[active]
                          ,[name]
                          ,[Family]
                          ,[FirstName]
                          ,[SecondName]
                          ,[Department_Name]
                          ,[Domain_Name]
                          ,[Job_Name]
                      FROM [ViewEmploys]
                      where active = 1""")

    for row in cursor.fetchall():
        department_name = row.Department_Name,
        domain_name = row.Domain_Name,
        job_name = row.Job_Name,

        if not row.Department_Name:
            department_name = ""
        if not row.Domain_Name:
            domain_name = ""
        if not row.Job_Name:
            job_name = ""
        if isinstance(department_name, tuple):
            department_name = department_name[0].replace("('", "").replace("',)", "")
        if isinstance(domain_name, tuple):
            domain_name = domain_name[0].replace("('", "").replace("',)", "")
        if isinstance(job_name, tuple):
            job_name = job_name[0].replace("('", "").replace("',)", "")

        conn.modify(
            'cn={0} {1} {2},cn=users,dc=,dc='.format(row.Family, row.FirstName, row.SecondName),
            {'description': [(MODIFY_REPLACE, ['Lynx_User:{0}'.format(row.Employ_ID)])]})
        print("Lynx<---->AD: {0} {1} {2}".format(row.Family, row.FirstName, row.SecondName))

    conn.search(ad_search_tree, '(&(objectCategory=organizationalPerson)(objectClass=User))', SUBTREE,
                attributes=['displayName', 'ipPhone', 'pager', 'mobile', 'mail', 'objectSid', 'sAMAccountName',
                            'userAccountControl', 'description']
                )

    for entry in conn.entries:
        try:
            display_name = str(entry['displayName']).split(" ")
            ip_phone = str(entry['ipPhone']).strip()
            mobile = str(entry['mobile']).strip().replace(" ", "").replace("-", "") \
                .replace("+7", "8").replace("(", "").replace(")", "")
            email = str(entry['mail']).strip()
            user_name = str(entry['sAMAccountName']).strip()
            is_disabled = str(entry['userAccountControl']).strip()
            employ_id = str(entry['description']).strip().split("Lynx_User:")
            employ_id = employ_id[1]

            Employ = UsersExtLynxEmploys.objects.get(employ_id=employ_id)

            if is_disabled == '66048':
                is_disabled = True
            if is_disabled == '66050':
                is_disabled = False

            if ip_phone == "[]":
                ip_phone = ""
            if mobile == "[]":
                mobile = ""
            if email == "[]":
                email = ""

            family = display_name[0].strip()
            first_name = display_name[1].strip()
            second_name = display_name[2].strip()

            delay = ""

            # добавляем быстрый набор с привязкой к стационарному номеру
            if ip_phone and mobile:
                for d in data:
                    d = d.split(",")
                    display_name_sip = d[2].strip().split("[")
                    employ_id_sip = display_name_sip[1].split("]")
                    employ_id_sip = employ_id_sip[0]
                    display_name_sip = display_name_sip[0].split("]")
                    display_name_sip = display_name_sip[0]
                    number_sip = d[0].strip()
                    speed_dial_sip = d[1].strip()

                    if employ_id_sip == employ_id:
                        dispn = display_name_sip.split(" ")

                        if number_sip != mobile or speed_dial_sip != ip_phone \
                                or dispn[0] != family or dispn[1] != first_name or dispn[2] != second_name:
                            delay = "update"
                            insert_or_update_sip(number_sip, speed_dial_sip, mobile, ip_phone, delay)
                        else:
                            delay = "pass"

                if delay == "":
                    delay = "insert"
                    insert_or_update_sip(number_sip, speed_dial_sip, mobile, ip_phone, delay)

            # ЗАПРОС в АД ПОСЛЕ ДОБАВЛЕНИЕ В БЫСТРЫЙ НАБОР ФРИБПХ
            if mobile != "" and ip_phone == "":
                speed_dial_array_FIL = []
                # speed_dial_d = []
                print(Employ.department_name)
                if Employ.department_name == "РТП Пятигорск":
                    first_dial_number = 5811
                    last_dial_number = 5899
                elif Employ.department_name == "РТП Волгоград":
                    first_dial_number = 5511
                    last_dial_number = 5599
                elif Employ.department_name == "РТП Краснодар":
                    first_dial_number = 5711
                    last_dial_number = 5799
                elif Employ.department_name == "РТП Новороссийск":
                    first_dial_number = 5411
                    last_dial_number = 5499
                elif Employ.department_name == "РТП Крым":
                    first_dial_number = 5611
                    last_dial_number = 5699
                elif Employ.department_name == "РТП Ставрополь":
                    first_dial_number = 5911
                    last_dial_number = 5999
                elif Employ.department_name == "РТП Махачкала":
                    first_dial_number = 5311
                    last_dial_number = 5399
                else:
                    first_dial_number = 200
                    last_dial_number = 299

                number_s = False
                delay = ""

                with urllib.request.urlopen("http://url/") as url2:
                    data_sip_actual = json.loads(url2.read().decode())
                for d2 in data_sip_actual:
                    d2 = d2.split(",")

                    display_name_sip2 = d2[2].strip().split("[")
                    employ_id2 = display_name_sip2[1].split("]")
                    employ_id2 = employ_id2[0]

                    number2 = d2[0].strip()
                    speed_dial2 = d2[1].strip()
                    if number2 == mobile:
                        number_s = True

                    # если изменился номер, обновляем
                    if employ_id2 == employ_id:
                        if number2 != mobile and number2 != "":
                            delay = "update"
                            insert_or_update_sip(number2, speed_dial2, mobile, speed_dial2, delay)
                        else:
                            delay = "pass"

                    speed_dial_array_FIL.append(int(speed_dial2))

                if not number_s:
                    if delay == "":
                        missingNumbers = list(
                            set(range(first_dial_number, last_dial_number + 1)) - set(speed_dial_array_FIL))
                        delay = "insert"
                        insert_or_update_sip("", "", mobile, missingNumbers[0], delay)

            def insert_or_update_sip(number_sip, speed_dial_sip, mobile, ip_phone, delay):
                if delay == "update":
                    print("Обновляем номер в FreePBX: {0} {1} {2}[{3}]:{4}".format(family, first_name, second_name,
                                                                                   Employ.employ_id, speed_dial_sip))
                    data_del = {"number": number_sip,
                                "cidname": "{0} {1} {2}[{3}]".format(family, first_name, second_name,
                                                                     Employ.employ_id),
                                "dial": speed_dial_sip,
                                "delay": "delete",
                                }
                    requests.post(url, data=json.dumps(data_del), headers=headers)
                    delay = "insert"

                if delay == "insert":
                    print("Добавляем номер в FreePBX: {0} {1} {2}[{3}]:{4}".format(family, first_name, second_name,
                                                                                   Employ.employ_id, speed_dial_sip))
                    data_ins = {"number": mobile,
                                "cidname": "{0} {1} {2}[{3}]".format(family, first_name, second_name,
                                                                     Employ.employ_id),
                                "dial": ip_phone,
                                "delay": "insert",
                                }
                    requests.post(url, data=json.dumps(data_ins), headers=headers)

        except Exception as ex:
            pass


def update_db(request):
    search_sql()
    search_ad()
    search_sip()

    return render(request, 'users_info/list.html', {
    })


def search_ad():
    global clearunit
    ad_server = ''
    ad_user = ad_user_admin
    ad_password = ad_password_admin
    ad_search_tree = 'dc=,dc='
    ad_users = []

    server = Server(ad_server, get_info=ALL)
    conn = Connection(server, user=ad_user, password=ad_password)
    conn.bind()

    conn.search(ad_search_tree, '(&(objectCategory=organizationalPerson)(objectClass=User))', SUBTREE,
                attributes=['displayName', 'ipPhone', 'pager', 'mobile', 'mail', 'objectSid', 'sAMAccountName',
                            'userAccountControl', 'description']
                )

    for entry in conn.entries:
        try:
            display_name = str(entry['displayName']).split(" ")
            ip_phone = str(entry['ipPhone']).strip()
            mobile = str(entry['mobile']).strip().replace(" ", "").replace("-", "") \
                .replace("+7", "8").replace("(", "").replace(")", "")
            phone_tablet = str(entry['pager']).strip().replace(" ", "").replace("-", "") \
                .replace("+7", "8").replace("(", "").replace(")", "")
            email = str(entry['mail']).strip()
            object_sid = str(entry['objectSid']).strip()
            user_name = str(entry['sAMAccountName']).strip()
            is_disabled = str(entry['userAccountControl']).strip()
            employ_id = str(entry['description']).strip().split("Lynx_User:")
            employ_id = employ_id[1]

            Employ = UsersExtLynxEmploys.objects.get(employ_id=employ_id)

            if is_disabled == '66048':
                is_disabled = True
            elif is_disabled == '66050':
                is_disabled = False
            elif is_disabled == '512':
                is_disabled = True
            else:
                print("Ошибка статуса: {0} {1} {2}".format(family, first_name, second_name))

            if ip_phone == "[]":
                ip_phone = ""
            if mobile == "[]":
                mobile = ""
            if phone_tablet == "[]":
                phone_tablet = ""
            if email == "[]":
                email = ""

            family = display_name[0].strip()
            first_name = display_name[1].strip()
            second_name = display_name[2].strip()

            UsersExtAD.objects.update_or_create(
                object_sid=object_sid,
                defaults={
                    'employ_id': Employ,
                    'display_name': "{0} {1} {2}".format(family, first_name, second_name),
                    'phone_local': ip_phone,
                    'phone_mobile': mobile,
                    'user_name': user_name,
                    'email': email,
                    'is_disabled': is_disabled
                },
            )
            if phone_tablet and is_disabled:
                UsersExtTablet.objects.update_or_create(
                    employ_id=Employ,
                    defaults={
                        'sim': phone_tablet,
                    },
                )
        except:
            pass

    return conn.entries


def search_sql():
    username = lynx_user_admin
    password = lynx_password_admin

    cursor = cnxn.cursor()

    cursor.execute("""
                    SELECT 
                           [Employ_ID]
                          ,[active]
                          ,[name]
                          ,[Family]
                          ,[FirstName]
                          ,[SecondName]
                          ,[Domain_ID]
                          ,[Department_Name]
                          ,[Domain_Name]
                          ,[Job_Name]
                          ,[DateOfEnd]
                      FROM [ViewEmploys]""")

    for row in cursor.fetchall():
        department_name = row.Department_Name
        domain_name = row.Domain_Name
        job_name = row.Job_Name
        vocation_date_of_begin = None
        vocation_date_of_end = None

        if not row.Department_Name:
            department_name = ""
        if not row.Domain_Name:
            domain_name = ""
        if not row.Job_Name:
            job_name = ""
        if isinstance(department_name, tuple):
            department_name = department_name[0].replace("('", "").replace("',)", "")
        if isinstance(domain_name, tuple):
            domain_name = domain_name[0].replace("('", "").replace("',)", "")
        if isinstance(job_name, tuple):
            job_name = job_name[0].replace("('", "").replace("',)", "")

        cursor.execute("""select top (1) DateOfBegin, dbo.GetDateOfEndVocation( DateOfBegin, Duration) as DateOfEnd
                    from Vacation_Order
                    where EmployTerm_ID = (select TermWork_ID 
                    from EmployEx
                    where Employ_ID = %s)
                    order by DateOfBegin desc""" % row.Employ_ID)

        for row2 in cursor.fetchall():
            vocation_date_of_begin = row2.DateOfBegin
            vocation_date_of_end = row2.DateOfEnd

        UsersExtLynxEmploys.objects.update_or_create(
            employ_id=row.Employ_ID,
            defaults={
                'name': row.name,
                'active': row.active,
                'family': row.Family,
                'first_name': row.FirstName,
                'second_name': row.SecondName,
                'domain_id': row.Domain_ID,
                'department_name': department_name,
                'domain_name': domain_name,
                'job_name': job_name,
                'vocation_date_of_begin': vocation_date_of_begin,
                'vocation_date_of_end': vocation_date_of_end,
            },
        )

    is_disabled = 1
    cursor.execute("""
                    select 
                         ViewUsers.user_id,
                         ViewUsers.user_name,
                         ViewUsers.user_active,
                         ViewUsers.user_type,
                         ViewUsers.Employ_id,
                         ViewEmploys.Domain_Name,
                         ViewUsers.Visor, 
                         ViewUsers.user_adm
                    from ViewUsers, ViewEmploys
                    where ViewUsers.user_type = 1 
                    and ViewUsers.Employ_id = ViewEmploys.Employ_ID""")

    for row in cursor.fetchall():
        try:
            Employ = UsersExtLynxEmploys.objects.get(employ_id=row.Employ_ID)
        except:
            Employ = None

        if Employ:
            if row.Visor is None:
                visor = ""
            else:
                visor = row.Visor

            if not row.Domain_Name:
                domain_name = ""
            else:
                domain_name = row.Domain_Name

            if row.user_active == 1:
                user_active = 0
            elif row.user_active == 0:
                user_active = 1

            UsersExtLynxUsers.objects.update_or_create(
                employ_id=Employ,
                defaults={
                    'user_id': row.user_id,
                    'user_name': row.user_name,
                    'is_disabled': user_active,
                    'domain_name': domain_name,
                    'user_type': row.user_type,
                    'visor': visor,
                    'adm_user': row.user_adm,
                },
            )

    cursor.execute("""select Rec_Id, Data_R from K_Sotr""")
    for row in cursor.fetchall():
        try:
            print(row.Rec_Id)
            UsersExtLynxEmploys.objects.update_or_create(
                employ_id=row.Rec_Id,
                defaults={
                    'birth_date': row.Data_R,
                },
            )
        except Exception:
            pass

    cursor = cnxn.cursor()

    last = ""
    cursor.execute("""Exec dbo.Os_CardLst "0R",1,0""")
    for row in cursor.fetchall():
        name = row.Name.strip()
        card_id = row.Card_Id.strip()
        invent_id = row.Invent_Id.strip()

        if row.NameMOL is not None:
            name_mol = row.NameMOL.strip()
        else:
            name_mol = ""

        if row.NamePodr is not None:
            name_podr = row.NamePodr.strip()
        else:
            name_podr = ""

        if row.Name_Ob is not None:
            name_ob = row.Name_Ob.strip()
        else:
            name_ob = ""

        if row.dps_Data is not None:
            dps_data = row.dps_Data
        else:
            dps_data = '1000-10-10 00:00:00'

        if row.cena_new is not None:
            cena_new = row.cena_new
        else:
            cena_new = ""

        try:
            Employ = UsersExtLynxEmploys.objects.get(name=name_mol)
            if Employ:
                UsersExtDBOS.objects.update_or_create(
                    invent_id=invent_id,
                    defaults={
                        'card_id': card_id,
                        'employ_id': Employ,
                        'name': name,
                        'name_ob': name_ob,
                        'name_podr': name_podr,
                        'name_mol': name_mol,
                        'cena_new': cena_new,
                        'dps_data': dateutil.parser.parse(str(dps_data) + '0')
                    },
                )
        except:
            if last != name_mol:
                last = name_mol

    def add_torg_ter(sql):
        cursor.execute(sql)
        for row in cursor.fetchall():

            if row.Name is not None:
                name_ter = row.Name.strip()
            else:
                name_ter = ""

            if row.FioTP is not None:
                name_tp = row.FioTP.strip()
            else:
                name_tp = ""

            if row.Comment is not None:
                full_ter = row.Comment.strip()
            else:
                full_ter = ""

            if row.People is not None:
                count = row.People
            else:
                count = ""

            try:
                name_tp = name_tp.split(" ")
                family = name_tp[0]
                first_name = name_tp[1]
                second_name = name_tp[2]

                match = None
                match = re.match(r"ДОН_", name_ter)
                if match:
                    Employ = UsersExtLynxEmploys.objects.get(family=family, first_name=first_name,
                                                             second_name=second_name)
                    UsersExtTablet.objects.update_or_create(
                        employ_id=Employ,
                        defaults={
                            'name_ter': name_ter,
                            'count': count,
                            'full_ter': full_ter,
                        },
                    )
            except:
                continue

    add_torg_ter("""[dbo].[K_TorgTerLst] '0R'""")  # ТТ ДОН
    add_torg_ter("""[dbo].[K_TorgTerLst] '0R', 1""")  # ТТ МИРМ


def search_sip():
    with urllib.request.urlopen("http://url/") as url:
        data = json.loads(url.read().decode())

        for d in data:
            d = d.split(",")
            display_name = d[2].strip().split("[")
            employ_id = display_name[1].split("]")
            employ_id = employ_id[0]
            display_name = display_name[0].split("]")
            display_name = display_name[0]

            number = d[0].strip()
            speed_dial = d[1].strip()
            if employ_id != "":
                Employ = UsersExtLynxEmploys.objects.get(employ_id=employ_id)
                ndb = UsersExtSIP.objects.filter(number=number)
                if ndb:
                    UsersExtSIP.objects.filter(number=number).delete()

                UsersExtSIP.objects.update_or_create(
                    employ_id=Employ,
                    defaults={
                        'number': number,
                        'display_name': display_name,
                        'speed_dial': speed_dial,
                    },
                )
