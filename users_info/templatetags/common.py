import re

from django.template import Library
from django import template
register = template.Library()


@register.filter_function
def order_by(queryset, args):
    print(queryset, args)
    args = [x.strip() for x in args.split(',')]
    return queryset.order_by(*args)


@register.filter
def size_trunk(queryset):
    queryset = str(queryset)
    queryset = queryset.replace("00:00:00", "")
    return queryset


@register.filter
def size_name(queryset):
    queryset = str(queryset[:70])
    return queryset


@register.filter
def size_name_os(queryset):
    queryset = str(queryset)
    queryset = match_s(queryset, 1)
    return queryset

