from django.contrib import admin
from django.db.models import TextField
from django.forms import Textarea

from django.db import models
from solo.admin import SingletonModelAdmin

from users_info.models import UsersExtLynxUsers, UsersExtLynxEmploys, UsersExtDBOS, UsersExtSIP, UsersExtAD, \
    UsersExtTablet, General, NamesInternetService, UsersExtInternetService


class DetalsUsersExtDBOS(admin.ModelAdmin):
    fields = [
        'employ_id',
        'card_id',
        'name',
        'name_podr',
        'name_mol',
        'invent_id',
        'name_ob',
        'cena_new',
        'dps_data',
    ]
    list_display = [
        'card_id',
        'invent_id',
        'name',
        'name_podr',
        'name_mol',
        'name_ob',
        'cena_new',
        'dps_data',
    ]


admin.site.register(UsersExtDBOS, DetalsUsersExtDBOS)


class DetalsUsersExtLynxUsers(admin.ModelAdmin):
    fields = [
        'user_id',
        'employ_id',
        'user_type',
        'user_name',
        'visor',
        'domain_name',
        'is_disabled',
        'adm_user',
    ]
    list_display = [
        'user_id',
        'employ_id',
        'user_type',
        'user_name',
        'visor',
        'domain_name',
        'is_disabled',
        'adm_user',
    ]


admin.site.register(UsersExtLynxUsers, DetalsUsersExtLynxUsers)


class DetalsUsersUsersExtLynxEmploys(admin.ModelAdmin):
    fields = [
        'employ_id',
        'name',
        'family',
        'first_name',
        'second_name',
        'domain_id',
        'department_name',
        'domain_name',
        'job_name',
        'date_of_end',
        'birth_date',
        'vocation_date_of_begin',
        'vocation_date_of_end',
        'active'
    ]
    list_display = [
        'employ_id',
        'name',
        'family',
        'first_name',
        'second_name',
        'domain_id',
        'department_name',
        'domain_name',
        'job_name',
        'date_of_end',
        'vocation_date_of_begin',
        'vocation_date_of_end',
        'active'
    ]


admin.site.register(UsersExtLynxEmploys, DetalsUsersUsersExtLynxEmploys)


class DetalsUsersExtSIP(admin.ModelAdmin):
    fields = [
        'employ_id',
        'display_name',
        'number',
        'speed_dial',
    ]
    list_display = [
        'employ_id',
        'display_name',
        'number',
        'speed_dial',
    ]


admin.site.register(UsersExtSIP, DetalsUsersExtSIP)


class DetalsUsersExtAD(admin.ModelAdmin):
    fields = [
        'object_sid',
        'employ_id',
        'user_name',
        'display_name',
        'phone_local',
        'phone_mobile',
        'email',
        'is_disabled',
    ]
    list_display = [
        'object_sid',
        'employ_id',
        'user_name',
        'display_name',
        'phone_local',
        'phone_mobile',
        'email',
        'is_disabled',
    ]


admin.site.register(UsersExtAD, DetalsUsersExtAD)


class DetalsNamesInternetService(admin.ModelAdmin):
    fields = [
        'name',
        'url',
    ]
    list_display = [
        'name',
        'url',
    ]


admin.site.register(NamesInternetService, DetalsNamesInternetService)


class DetalsUsersExtInternetService(admin.ModelAdmin):
    fields = [
        'employ_id',
        'name_services',
    ]
    list_display = [
        'employ_id',
    ]


admin.site.register(UsersExtInternetService, DetalsUsersExtInternetService)


class DetalsUsersExtTablet(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 40})},
    }

    fields = [
        'employ_id',
        'sim',
        'comment',
        'name_ter',
        'full_ter',
        'count',
    ]
    list_display = [
        'employ_id',
        'sim',
        'comment',
        'name_ter',
        'full_ter',
        'count',
    ]


admin.site.register(UsersExtTablet, DetalsUsersExtTablet)

admin.site.register(
    General,
    SingletonModelAdmin,
    fields=[
        'general_groups_admin',
        'general_groups',
    ]
)
