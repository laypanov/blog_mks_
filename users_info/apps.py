from django.apps import AppConfig


class Users_InfoConfig(AppConfig):
    name = 'users_info'
    verbose_name = "Внешние источники данных"
