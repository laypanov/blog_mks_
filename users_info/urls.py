from django.conf.urls import url
from users_info import views

app_name = 'users_info'

urlpatterns = [
    url(r'^$', views.view_tablets, name='view_tablets'),
    url(r'^update_db', views.update_db, name='update_db'),
    url(r'^sync_db', views.sync_db, name='sync_db'),
]
