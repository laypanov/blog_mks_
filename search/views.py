from itertools import chain

from django.shortcuts import render
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.views import View

from handbook.models import Post
from regulations.models import Post as PostRegulations
from blog_mks.utils import group_required_post


class ESearchView(View):
    template_name = 'search/main.html'

    def get(self, request, *args, **kwargs):
        context = {}

        q = request.GET.get('q')
        if q:
            query_sets = []  # Общий QuerySet

            # Ищем по всем моделям
            #query_sets.append(Article.objects.search(query=q))
            #query_sets.append(Comment.objects.search(query=q))
            #query_sets.append(Tag.objects.search(query=q))

            creators_list = group_required_post(request)
            query_sets.append(Post.objects.search(query=q).filter(post_groups__name__in=creators_list,
                                                                  published=True).distinct())
            query_sets.append(PostRegulations.objects.search(query=q).filter(post_groups__name__in=creators_list,
                                                                             published=True).distinct())

            # и объединяем выдачу
            final_set = list(chain(*query_sets))
            final_set.sort(key=lambda x: x.title, reverse=True)  # Выполняем сортировку

            context['last_question'] = '?q=%s' % q

            current_page = Paginator(final_set, 10)

            page = request.GET.get('page')
            try:
                context['object_list'] = current_page.page(page)
            except PageNotAnInteger:
                context['object_list'] = current_page.page(1)
            except EmptyPage:
                context['object_list'] = current_page.page(current_page.num_pages)

        return render(request=request, template_name=self.template_name, context=context)

