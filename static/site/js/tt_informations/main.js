$(document).ready(function() {
    var csrftoken = getCookie('csrftoken')

    $( ".change_status" ).click(function() {
    var resultActionUser = confirm("Изменить статус?");
        if (resultActionUser) {
            if ($(".change_status").text() == "Нет") cs = 1
            if ($(".change_status").text() == "Да")  cs = 0
            author_uuid = $("#author_uuid").text()
            $.ajax({
                    type:'POST',
                    url: '/tt_informations/question/change_status/',
                    dataType: 'json',
                    data: {
                            csrfmiddlewaretoken: csrftoken,
                            rec_id: $( "#rec_id" ).text(),
                            cs: cs,
                            author_uuid: author_uuid
                    },
                    success: function(data){
                        if (data == "done"){
                            location.reload();
                        }
                    }
                });
        }else{
        }
    });

    $('#ControlTextarea').bind('input propertychange', function() {

      $(".save_textarea").show();

      if(this.value.length){
        $("#yourBtnID").show();
      }
    });

    $( ".save_textarea" ).click(function() {
        author_uuid = $("#author_uuid").text()
        $.ajax({
                type:'POST',
                url: '/tt_informations/question/save_textarea/',
                dataType: 'json',
                data: {
                        csrfmiddlewaretoken: csrftoken,
                        rec_id: $("#rec_id").text(),
                        author_uuid: author_uuid,
                        textarea: $("#ControlTextarea").val(),
                },
                success: function(data){
                    if (data == "done"){
                        location.reload();
                    }
                }
            });
    });


    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }


});