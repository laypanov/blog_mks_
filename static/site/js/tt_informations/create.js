$(document).ready(function() {
    var last_marker = new L.marker(47.228615, 39.675751);
    var create = 0;

    var mymap = L.map('mapid').setView([$('#id_latitude').val(), $('#id_longitude').val()], 15);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 20,
    id: '',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: ''
    }).addTo(mymap);

    const provider = new window.GeoSearch.HereProvider({
        params: {
            apiKey: 'rFtHcZkQy8q3Exr9B_ZR1nsM8FLQ1Ueaycg-V6T-l5k',
            'accept-language': 'ru',
        },
    });
    const search = new GeoSearch.GeoSearchControl({
        provider: provider,
        style: 'bar',
        updateMap: true,
        searchLabel: 'Введите адрес',
        language: 'ru',
        countrycodes: 'ru',
    });
    search.addTo(mymap);


    mymap.on('geosearch/showlocation', function(result) {
        $('#id_address').val(result['location']['raw']['address']['street'] + ', ' + result['location']['raw']['address']['houseNumber'])
        $('#id_town').val(result['location']['raw']['address']['city'])
        $('#id_region').val(result['location']['raw']['address']['county'])
        $('#id_full_address').val(result['location']['raw']['address']['label'])
        $('#id_latitude').val(result['location']['y'])
        $('#id_longitude').val(result['location']['x'])
    });

    //mymap.on('geosearch/showlocation', alert(1));

    mymap.on('click', function(ev) {
        last_marker.remove()
        var marker = new L.marker(ev.latlng).addTo(mymap);
        $('#id_longitude').val(ev['latlng']['lng'].toFixed(6))
        $('#id_latitude').val(ev['latlng']['lat'].toFixed(6))
        last_marker = marker
    });


    str = "";
    $("#id_kanal option:selected").each(function() {
        str += $( this ).text() + " ";
        if(str == "Выберите... "){
            create = 1;
        }else{
            create = 0;
            $('#id_sostav').prop( "disabled", true );
            var marker = L.marker([$('#id_latitude').val(), $('#id_longitude').val()]).addTo(mymap);
            last_marker.remove()
            last_marker = marker
        }
    });

    // крч надо разобартсья со статусами create???
    $("#id_kanal").change(function () {
        var str = "";
        $("#id_kanal option:selected").each(function() {
            if(create == 1){
                str += $( this ).text() + " ";
                $('#id_sostav').prop( "disabled", false );

                $('#id_sostav')
                    .find('option')
                    .remove()
                    .end()
            }
        });

        if(str == 'Бизнес для бизнеса '){
            function populate(selector) {
                $(selector)
                    .append('<option selected value="">Выберите...</option>')
                    .append('<option value="Автопредприятие">Автопредприятие</option>')
                    .append('<option value="Автосервис">Автосервис</option>')
                    .append('<option value="ЖКХ">ЖКХ</option>')
                    .append('<option value="Заводы фабрики">Заводы фабрики</option>')
                    .append('<option value="Образовательные учереждения">Образовательные учереждения</option>')
                    .append('<option value="Ремонтные бригады">Ремонтные бригады</option>')
                    .append('<option value="Силовые структуры">Силовые структуры</option>')
                    .append('<option value="Строительные компании">Строительные компании</option>')
                    .append('<option value="Транспорт">Транспорт</option>')
            }
            populate('#id_sostav');
        }else if(str == 'Магазины '){
            function populate(selector) {
                $(selector)
                    .append('<option selected value="">Выберите...</option>')
                    .append('<option value="Авто">Авто</option>')
                    .append('<option value="Инструмент">Инструмент</option>')
                    .append('<option value="Интернет магазин">Интернет магазин</option>')
                    .append('<option value="Лаки и краски">Лаки и краски</option>')
                    .append('<option value="Маркет">Маркет</option>')
                    .append('<option value="Метизы">Метизы</option>')
                    .append('<option value="Прочие">Прочие</option>')
                    .append('<option value="Садовая">Садовая</option>')
                    .append('<option value="Сантехника">Сантехника</option>')
                    .append('<option value="Строительные материалы">Строительные материалы</option>')
                    .append('<option value="Хозяйственные материалы">Хозяйственные материалы</option>')
                    .append('<option value="Электроинструмент">Электроинструмент</option>')
                    .append('<option value="Электрофурнитура">Электрофурнитура</option>')
            }
            populate('#id_sostav');
        }else if(str == 'ОПТ '){
            function populate(selector) {
                $(selector)
                    .append('<option selected value="">Выберите...</option>')
                    .append('<option value="Оптовик">Оптовик</option>')
                    .append('<option value="Посредник">Посредник</option>')
                    .append('<option value="Прочие">Прочие</option>')
            }
            populate('#id_sostav');
        }else if(str == 'Рынки '){
            function populate(selector) {
                $(selector)
                    .append('<option selected value="">Выберите...</option>')
                    .append('<option value="Розница рынки">Розница рынки</option>')
            }
            populate('#id_sostav');
        }else if(str == 'Сети '){
            function populate(selector) {
                $(selector)
                    .append('<option selected value="">Выберите...</option>')
                    .append('<option value="Не специализированные_региональные">Не специализированные_региональные</option>')
                    .append('<option value="Не специализированные_федеральные">Не специализированные_федеральные</option>')
                    .append('<option value="Не специализированные(DIY)_региональные">Не специализированные(DIY)_региональные</option>')
                    .append('<option value="Не специализированные(DIY)_региональные">Не специализированные(DIY)_региональные</option>')
                    .append('<option value="Не специализированные(DIY)_федеральные">Не специализированные(DIY)_федеральные</option>')
            }
            populate('#id_sostav');
        }

        create = 1;
      })
      .change();

    $( ".sub_ce" ).click(function() {
        $('#id_sostav').prop( "disabled", false );
    });

});