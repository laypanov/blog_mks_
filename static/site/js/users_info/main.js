$(document).ready(function() {
    $(document).on("click", ".view_adv", function() {
        td = $(this).text()
        if (td == "+"){
            $(this).html("-")
            $(this).parents("tr").next("tr").show()
            $(this).parents("tr").addClass("top_full_view_block");
            $(this).parents("tr").next("tr").addClass("full_view full_view_block");
            $(this).parents("tr").removeClass("simple_view");
        }else{
            $(this).html("+")
            $(this).parents("tr").next("tr").hide()
            $(this).parents("tr").removeClass("top_full_view_block");
            $(this).parents("tr").next("tr").removeClass("full_view_block");
            $(this).parents("tr").addClass("simple_view");
        }
    });
    // ДР
    $("td.birth_now").parents('tr').css("background", "#fff");
    $("td.birth_now").parents('tr').css("color", "#496edb");
    // отпуск
    $("td.vocation_now").parents('tr').css("background", "#fff");

    $('.table').DataTable();

});