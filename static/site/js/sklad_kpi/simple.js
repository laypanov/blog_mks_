function scroll_down() {
    $('html, body').animate({scrollTop: '+=' + ($(window).height() - 70)}, 1000);
}
timer_down = setInterval(scroll_down, 15000);

function scroll_up() {
    $('html, body').animate({scrollTop: '-=' + ($(window).height() - 70)}, 1000);
}

function refresh_page() {
    current_url = $(location).attr('href');
    window.location.replace(current_url);
}
setInterval(refresh_page, 900000);

var timer_down;
var timer_up;

$(window).scroll(function() {
   // верх
   if ($(window).scrollTop() == 0){
        console.log('down')
        clearInterval(timer_up);
        timer_down = setInterval(scroll_down, 15000);
   }
   // низ страницы
   if ($(window).scrollTop() + $(window).height() == $(document).height()) {
       console.log('up')
       clearInterval(timer_down);
       timer_up = setInterval(scroll_up, 15000);
   }
});

$('.table').stickyTableHeaders();