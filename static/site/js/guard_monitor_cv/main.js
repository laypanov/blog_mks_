$(document).ready(function() {
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    var d = new Date();

    var currDate = d.getDate();
    var currMonth = d.getMonth();
    var currYear = d.getFullYear();

    var dateStr = currDate + "-" + currMonth + "-" + currYear;

    $('#datepicker').datepicker({
        todayBtn: "linked",
        language: "ru",
        format: 'dd.mm.yyyy',
        todayHighlight: true,
        defaultDate: dateStr,
        showButtonPanel: true
    });
});
