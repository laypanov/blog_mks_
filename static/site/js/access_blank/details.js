$(document).ready(function() {
    pathname = $(location).attr('pathname');
    p = pathname.split('/')
    if (p[2] == 'new'){
        $("#inp_name").val("")
        $("#inp_mail").val("")
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken')

    $(".get_employ_search1").select2({
        ajax: {
            type:'POST',
            url: '/access_blank/post/get_employ_search2_ajax',
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    csrfmiddlewaretoken: csrftoken,
                    post_data: params.term
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 10) < data.count_filtered
                    }
                };
            },
            cache: true
        },
        minimumInputLength: 3,
        placeholder: "ФИО нового сотрудника",
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: function (d) {
            if (d.loading==true){
                return '<span>Поиск...</span>';
            }else{
                return '<span>' + d.text + '</span>' +
                '<span>' + d.department_name + '</span>' +
                '<span>' + d.employ_id + '</span>';
            }
        },
        templateSelection: function (d) {
            return d.text;
        },language: {
            inputTooShort: function (d) {
                var remainingChars = d.minimum - d.input.length;
                var message = 'Пожалуйста, введите ' + remainingChars + ' или более символов';
                return message;
            },
            noResults: function () {
                return 'Ничего не найдено';
            },
        }
    }).val(null).trigger("change");

    $(".get_employ_search2").select2({
        ajax: {
            type:'POST',
            url: '/access_blank/post/get_employ_search2_ajax',
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    csrfmiddlewaretoken: csrftoken,
                    post_data: params.term
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 10) < data.count_filtered
                    }
                };
            },
            cache: true
        },
        minimumInputLength: 3,
        placeholder: "Предоставить доступ как у сотрудника",
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: function (d) {
            if (d.loading==true){
                return '<span>Поиск...</span>';
            }else{
                return '<span>' + d.text + '</span>' +
                '<span>' + d.department_name + '</span>' +
                '<span>' + d.employ_id + '</span>';
            }
        },
        templateSelection: function (d) {
            return d.text;
        },language: {
            inputTooShort: function (d) {
                var remainingChars = d.minimum - d.input.length;
                var message = 'Пожалуйста, введите ' + remainingChars + ' или более символов';
                return message;
            },
            noResults: function () {
                return 'Ничего не найдено';
            },
        }
    }).val(null).trigger("change");


});
