$(document).ready(function() {
    var csrftoken = getCookie('csrftoken')
    var data_rem = []
    var data_added = []

    $( "#add_podr_a" ).click(function() {
        $(".add_podr").show();
        $("#add_podr_a").parent().hide();
    });

    $( ".plus" ).click(function() {
        $("#data_upodr").show()
        podr = $(this).parent().parent().find("th").eq(0).text()
        $("#data_upodr #id_podr option").filter(function() {
          return $(this).text() == podr;
        }).prop('selected', true);
    });

    $( ".minus" ).click(function() {
        podr = $(this).parent().parent().parent().find("th").eq(0).text()
        employ_id = $(this).parent().find("td").eq(0).text()
        $.ajax({
            type: "POST",
            url: "upodr_del/",
            data: {
                csrfmiddlewaretoken: csrftoken,
                podr: podr,
                employ_id: employ_id,
            },
            success: function(data){
                location.reload();
            }
        });
    });

    $( ".podr_minus" ).click(function() {
        podr = $(this).parent().parent().parent().find("th").eq(0).text()
        $.ajax({
            type: "POST",
            url: "podr_del/",
            data: {
                csrfmiddlewaretoken: csrftoken,
                podr: podr,
            },
            success: function(data){
                location.reload();
            }
        });
    });

    $( "#id_podr" ).change(function() {
        $(".upodr_adv tbody tr").remove("");

        podr = $("#id_podr option:selected").text()
        $.ajax({
        type: "POST",
        url: "get_upodr/",
        data: {
            csrfmiddlewaretoken: csrftoken,
            podr: podr,
        },
        success: function(data){
            if (data.length > 0){
                var val = 0
                for (const prop in data) {
                    row = '<tr><td>'+ data[val] +'</td><td class="minus text-right">-</td></tr>'
                    $('.upodr_adv > tbody:last').append(row);
                    val += 1
                }

            }
        }
        });
    });

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

});

