$(document).ready(function() {
    var al = 0;
    var csrftoken = getCookie('csrftoken')


    $(".adv_sign_user_gr").click(function() {
        employ_id = $("#id_employ_id option:selected").text()
        if (employ_id != "---------"){
            $('.upodr_adv tbody  > tr').each(function(index, tr) {
                    if (employ_id == tr['textContent'].replace("-", "")){
                        alert('Добавлено ранее')
                        al = 1
                    }
            });

            if (al != 1){
                row = '<tr><td>'+ employ_id +'</td><td class="minus text-right">-</td></tr>'
                $('.upodr_adv > tbody:last').append(row);
            }else{
                al = 0
            }
        }
    });

    $( ".upodr_adv" ).on( "click", ".minus", function() {
        $(this).parent().remove()
    });

    $(".user_info #id_employ_id").change(function() {
        $(".log_rem").remove()
        employ_id = $('.user_info #id_employ_id  option:selected').text()
        $.ajax({
            type: "POST",
            url: "get_date_of_end/",
            data: {
                csrfmiddlewaretoken: csrftoken,
                employ_id: employ_id,
            },
            success: function(data){
                console.log(data)
                if(data != "None"){
                   d = data.split("-")
                   $("#id_date_dism").val(""+data+"")
                   $('.user_info tbody').append("<tr class='log_rem'><td colspan='3'>Данные об увольнении найдены</td></tr>");
                }else{
                    var d = new Date();

                    var month = d.getMonth()+1;
                    var day = d.getDate();
                    var hour = d.getHours();
                    var minute = d.getMinutes();
                    var second = d.getSeconds();
                    d.getFullYear()
                    $("#id_date_dism").val(
                        '' + d.getFullYear() + '-' +
                        ((''+month).length<2 ? '0' : '') + month + '-' +
                        ((''+day).length<2 ? '0' : '') + day + '')
                    //$("#id_date_dism").val("1000-01-01")
                    //$('.user_info tbody').append("<tr class='log_rem'><td colspan='3'>Нет записей о дате увольнения в БД!</td></tr>");
                }
            }
        });
    });

    $(".create_new").click(function() {
        date_dism = $('#id_date_dism').val()
        employ = $('.user_info #id_employ_id  option:selected').text()
        sing_users = []
        $('.upodr_adv tbody  > tr').each(function(index, tr) {
            sing_users.push(tr['textContent'].replace("-", ""));
        });
        if (sing_users.length > 0 && employ != "---------" && date_dism != '1000-01-01'){
            sing_users = JSON.stringify(sing_users);
            $.ajax({
                type: "POST",
                url: "create_new/",
                data: {
                    csrfmiddlewaretoken: csrftoken,
                    employ: employ,
                    date_dism: date_dism,
                    sing_users: sing_users,
                },
                success: function(data){
                    if(data == 'None'){
                        alert("Ошибка при создании")
                    }else{
                        window.location.replace(window.location.origin+"/hr/ol/details/"+ data +"/");
                    }
                }
            });
        }else{
            alert('Ошибка, проверьте корректность заполнения')
        }
    });

    $( ".plus" ).click(function() {
        $(".data_upodr").show()
    });

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

});

