$(document).ready(function() {
    pathname = $(location).attr('pathname');
    p = pathname.split('/')
    if (p[2] == 'new'){
        $("#inp_name").val("")
        $("#inp_mail").val("")
    }
    var csrftoken = getCookie('csrftoken')
    var data_rem = []
    var data_added = []
    var result_reg_nomer;

    $(document).on("input", ".tdlimit", function() {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    // если удалили объект, удаляем его из таблицы
    $('.mks_search2_ajax').on("select2:unselect", function(e) {
        client_id = e.params.data.Client_id;
        var td = $("td:contains("+client_id+")");
        td.parents("tr").remove();
    });

    $('.mks_search2_ajax').on('select2:selecting', function (e) {
        var data = e.params.args.data;
        var Limit_Rez = data.Limit_Rez;

        $("#btdo").attr("hidden",false);
        data_added.push(data.Client_id)
        if (Limit_Rez === '' || Limit_Rez === null){
           Limit_Rez = 0
        }

        p = pathname.split('/')
        if (p[2] == 'new'){
            if ($('#tcreg tbody tr').length == 0){
                $('#tcreg tbody:last-child').append(
                    "<tr>" +
                    "<td class='tdclientid'>"+data.Client_id+"</td>" +
                    "<td class='tdtnetid'>"+data.Net_ID+"</td>" +
                    "<td class='tdnetname'>"+data.Net_name+"</td>" +
                    "<td class='tdmembername'>"+data.Member_name+"</td>" +
                    "<td class='tdcontact'>"+data.contact+"</td>" +
                    "<td id='tdclrezerv_"+data.Client_id+"'><div class='spinner-grow spinner-border-sm' role='status'></div></td>" +
                    "<td class='tdlimitro' id='tdlimitro_"+data.Client_id+"'>"+Limit_Rez+"</td>" +
                    "<td><input class='tdlimit' value='"+Limit_Rez+"' maxlength='7' max='9999999' type='number' pattern='[0-9]+' name='limit'/></td>" +
                    "</tr>"
                )
            }else{
                next = 0
                $('#tcreg tbody tr').each(function (i, el) {
                    if (next == 1) return true;
                    if (el['childNodes']['1']){
                        tdtnetid = $(this).find(".tdtnetid").html();
                    }
                    if (tdtnetid == data.Net_ID) {
                        $(this).find(".tdtnetid").parent().after(
                            "<tr>" +
                            "<td class='tdclientid'>"+data.Client_id+"</td>" +
                            "<td class='tdtnetid'>"+data.Net_ID+"</td>" +
                            "<td class='tdnetname'>"+data.Net_name+"</td>" +
                            "<td class='tdmembername'>"+data.Member_name+"</td>" +
                            "<td class='tdcontact'>"+data.contact+"</td>" +
                            "<td id='tdclrezerv_"+data.Client_id+"'><div class='spinner-grow spinner-border-sm' role='status'></div></td>" +
                            "<td class='tdlimitro' id='tdlimitro_"+data.Client_id+"'>"+Limit_Rez+"</td>" +
                            "<td></td>" +
                            "</tr>"
                        )
                        next = 1
                    }else if (i == $('#tcreg tbody tr').length -1){
                        $('#tcreg tbody:last-child').append(
                                "<tr>" +
                                "<td class='tdclientid'>"+data.Client_id+"</td>" +
                                "<td class='tdtnetid'>"+data.Net_ID+"</td>" +
                                "<td class='tdnetname'>"+data.Net_name+"</td>" +
                                "<td class='tdmembername'>"+data.Member_name+"</td>" +
                                "<td class='tdcontact'>"+data.contact+"</td>" +
                                "<td id='tdclrezerv_"+data.Client_id+"'><div class='spinner-grow spinner-border-sm' role='status'></div></td>" +
                                "<td class='tdlimitro' id='tdlimitro_"+data.Client_id+"'>"+Limit_Rez+"</td>" +
                                "<td><input class='tdlimit' value='"+Limit_Rez+"' maxlength='7' max='9999999' type='number' pattern='[0-9]+' name='limit'/></td>" +
                                "</tr>"
                        )
                    }
                })
            }
            color_rt()
        }else if (p[2] == 'details'){
            tdclientid_first = $('#tcreg tbody tr:eq(0)').find(".tdclientid").html()
            tdclientid = $('#tcreg tbody tr:eq(0)').find(".tdclientid").html()
            if (($('#tcreg tbody tr').length == 0) && (tdclientid_first != data.Client_id)){
                $('#tcreg tbody:last-child').append(
                    "<tr class=new_edit>" +
                    "<td><input type='checkbox' checked name='record'>&nbsp;</td>" +
                    "<td class='tdclientid'>"+data.Client_id+"</td>" +
                    "<td class='tdtnetid'>"+data.Net_ID+"</td>" +
                    "<td class='tdnetname'>"+data.Net_name+"</td>" +
                    "<td class='tdmembername'>"+data.Member_name+"</td>" +
                    "<td class='tdcontact'>"+data.contact+"</td>" +
                    "<td id='tdclrezerv_"+data.Client_id+"'><div class='spinner-grow spinner-border-sm' role='status'></div></td>" +
                    "<td class='tdlimitro' id='tdlimitro_"+data.Client_id+"'>"+Limit_Rez+"</td>" +
                    "<td><input class='tdlimit' value='"+Limit_Rez+"' maxlength='7' max='9999999' type='number' pattern='[0-9]+' name='limit'/></td>" +
                    "</tr>"
                )
            }else{
                next = 0
                $('#tcreg tbody tr').each(function (i, el) {
                    if (el['childNodes']['1']){
                        tdtnetid = $(this).find(".tdtnetid").html();
                        tdclientid = $(this).find(".tdclientid").html();
                    }

                     if (tdclientid == data.Client_id){
                        next = 1
                        return true;
                    }
                })
                $('#tcreg tbody tr').each(function (i, el) {
                    if (el['childNodes']['1']){
                        tdtnetid = $(this).find(".tdtnetid").html();
                        tdclientid = $(this).find(".tdclientid").html();
                    }

                    if (next == 1) return true;
                    if (tdtnetid == data.Net_ID){
                        $(this).find(".tdtnetid").parent().after(
                            "<tr class=new_edit>" +
                            "<td><input type='checkbox' checked name='record'>&nbsp;</td>" +
                            "<td class='tdclientid'>"+data.Client_id+"</td>" +
                            "<td class='tdtnetid'>"+data.Net_ID+"</td>" +
                            "<td class='tdnetname'>"+data.Net_name+"</td>" +
                            "<td class='tdmembername'>"+data.Member_name+"</td>" +
                            "<td class='tdcontact'>"+data.contact+"</td>" +
                            "<td id='tdclrezerv_"+data.Client_id+"'><div class='spinner-grow spinner-border-sm' role='status'></div></td>" +
                            "<td class='tdlimitro' id='tdlimitro_"+data.Client_id+"'>"+Limit_Rez+"</td>" +
                            "<td></td>" +
                            "</tr>"
                        )
                        next = 1
                    }else if (i == $('#tcreg tbody tr').length -1 ) {
                        $('#tcreg tbody:last-child').append(
                                "<tr class=new_edit>" +
                                "<td><input type='checkbox' checked name='record'>&nbsp;</td>" +
                                "<td class='tdclientid'>"+data.Client_id+"</td>" +
                                "<td class='tdtnetid'>"+data.Net_ID+"</td>" +
                                "<td class='tdnetname'>"+data.Net_name+"</td>" +
                                "<td class='tdmembername'>"+data.Member_name+"</td>" +
                                "<td class='tdcontact'>"+data.contact+"</td>" +
                                "<td id='tdclrezerv_"+data.Client_id+"'><div class='spinner-grow spinner-border-sm' role='status'></div></td>" +
                                "<td class='tdlimitro' id='tdlimitro_"+data.Client_id+"'>"+Limit_Rez+"</td>" +
                                "<td><input class='tdlimit' value='"+Limit_Rez+"' maxlength='7' max='9999999' type='number' pattern='[0-9]+' name='limit'/></td>" +
                                "</tr>"
                        )
                    }
                })
            }
        }

        $("#fixedh").show();
        if (data.Member_name){
            inp_name = $("#inp_name").val()
            if (inp_name == ''){
                $("#inp_name").val(""+data.Member_name+"")
            }
        } else{
            //$("#inp_mail").val("Необходимо актуализировать")
        }

        if (data.mail){
            inp_mail = $("#inp_mail").val()
            if (inp_mail == ''){
                $("#inp_mail").val(""+data.mail+"")
            }
        } else{
            //$("#inp_mail").val("Необходимо актуализировать")
        }

        $.ajax({
                type:'POST',
                url: '/mks_editor/post/get_clrezerv',
                dataType: 'json',
                data: {
                        csrfmiddlewaretoken: csrftoken,
                        data: data.Client_id,
                },
                beforeSend: function( xhr ) {
                },
                success: function(data_out){
                    if (data_out){
                        data_out = data_out.split(',')
                        cl_id = data_out[0]
                        clrezerv = data_out[1]
                        Limit_Rez_Old = $('#tdlimitro_' + cl_id).html();
                        if (parseInt(Limit_Rez_Old) < parseInt(clrezerv)){
                            $('#tdlimitro_' + cl_id).addClass('red');
                        }
                    $('#tdclrezerv_' + cl_id).text(clrezerv)
                    } else {
                        $("#btdonew").text('Ошибка');
                    }
                }
        });
    });

    $(".mks_search2_ajax").select2({
        ajax: {
            type:'POST',
            url: '/mks_editor/post/mks_search2_ajax',
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    csrfmiddlewaretoken: csrftoken,
                    post_data: params.term // search term
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 10) < data.count_filtered
                    }
                };
            },
            cache: true
        },
        minimumInputLength: 3,
        placeholder: "Добавить клиента в список: по названию, коду клиента, группе компаний, менеджеру",
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: function (d) {
            if (d.loading==true){
                return '<span>Поиск...</span>';
            } else
                Member_name = d.Member_name
            if(d.block == true){
                return  '<span class="red">' + Member_name + '</span><span class="red"> '+ d.Net_name +' </span><span class="red">[ '+ d.Client_id +' ]</span><span class="red">' + d.contact + '</span>';
            }else{
                return '<span>' + Member_name + '</span><span> '+ d.Net_name +' </span><span>[ '+ d.Client_id +' ]</span><span>' + d.contact + '</span>';
            }
        },
        templateSelection: function (d) {
            return d.text;
        },language: {
            inputTooShort: function (d) {
                var remainingChars = d.minimum - d.input.length;
                var message = 'Пожалуйста, введите ' + remainingChars + ' или более символов';
                return message;
            },
            noResults: function () {
                return 'Ничего не найдено';
            },
        }
    }).val('test2').trigger('change');

    $('#tcreg').on('click', 'tbody td', function(){
        $("#tcreg").find('input[name="record"]').each(function(){
            if($(this).prop('checked')) {
                }else{
                    r = $(this).parents("tr").html().split("<td>")
                    r = r[2].split("</td>")
                    console.log($(this).parents("tr").find(".tdclientid").html())
                    Client_id = $(this).parents("tr").find(".tdclientid").html();
                    data_rem.push({Client_id: Client_id})
                    $(this).parents("tr").remove();
                    $(".mks_search2_ajax").val(null).trigger("change");
                }
        });
    });

    $("#fastsearch").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#tbfs tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    // выравниваем таблицу по высоте, если будет сверх больше значение
    doc_h = $(window).height();
    doc_navbar = $(".static-top").height();
    doc_footer = $(".sticky-footer").height();
    doc_table = doc_h-doc_navbar-doc_footer - 350
    $('#fixedh').css('max-height', doc_table + "px");

    $( "#btdtogo" ).click(function() {
        window.location.replace("/mks_editor/details/"+result_reg_nomer.trim());
    });

    $( "#btdcreateagain" ).click(function() {
        window.location.reload(true)
    });

    $( "#btdonew" ).click(function() {
        data_added = []
        var Limit_Rez;
        var Member_name;
        var Client_id;
        var Limit_Rez;
        var clear = true
        $("#inp_name").val($.trim($("#inp_name").val()))
        $("#inp_mail").val($.trim($("#inp_mail").val()))

        if ($('#tcreg tbody tr').length > 0){

                email = $("#inp_mail").val();
                function isEmail(email) {
                  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                  return regex.test(email);
                }
                if (isEmail(email) == false){
                   alert('Некорректно указан email клиента');
                   clear = false
                }

            tdtnetid_old2 = ""
            $('#tcreg tbody tr').each(function (i, el) {

                member_name = $("#inp_name").val();

                if (el['childNodes']['1']){
                    Client_id = $(this).find(".tdclientid").html();


                    tdtnetid = $(this).find(".tdtnetid").html();
                    if (i == 0){
                        Limit_Rez = $(this).find("td").find(".tdlimit").val();
                        Limit_Rez_Old = $(this).find(".tdlimitro").html();
                    }else{
                        if (tdtnetid_old2 == tdtnetid){
                        }else{
                            Limit_Rez = $(this).find("td").find(".tdlimit").val();
                            Limit_Rez_Old = $(this).find(".tdlimitro").html();
                        }
                    }
                    if (Limit_Rez === '' || Limit_Rez == 0 || Limit_Rez[0] == 0){
                       Limit_Rez = 100000
                       $(this).find("td").find(".tdlimit").val(100000)
                    }
                    if (Limit_Rez.length > 8 ){
                        Limit_Rez = 15000000;
                       $(this).find("td").find(".tdlimit").val(15000000)
                    }
                    if (Limit_Rez_Old === '' || Limit_Rez_Old == 0 || Limit_Rez_Old == 'None'){
                       Limit_Rez_Old = 0
                    }

                    data_added.push({
                        Client_id: Client_id,
                        Limit_Rez: Limit_Rez,
                        Limit_Rez_Old: Limit_Rez_Old,
                    });
                }
                tdtnetid_old2 = tdtnetid
            });

            if (clear){
                data_json = JSON.stringify(data_added);
                data_rem = JSON.stringify(data_rem);

                count = $('#tcreg tbody tr').length,

                $.ajax({
                    type:'POST',
                    url: '/mks_editor/post/mks_reg_create',
                    dataType: 'json',
                    data: {
                            csrfmiddlewaretoken: csrftoken,
                            data_added: data_json,
                            email: email,
                            member_name: member_name,
                            count: count,
                            pathname: pathname,
                            data_rem: data_rem
                    },
                    beforeSend: function( xhr ) {
                        $("#btdonew").prop('disabled', true);
                        $("#btdonew").text('Выполнение...');
                    },
                    success: function(data){
                        result = data.split(',')
                        if (result[0] == "done"){
                            $("#btdonew").text('Выполнено');
                            $("#btdcreateagain").show()
                            $("#btdtogo").show()

                            p = pathname.split('/')
                            result_reg_nomer = result[1]
                            if (p[2] == 'details'){
                                setTimeout(function(){
                                    location.reload(true)
                                    window.location.replace("/mks_editor/details/"+result_reg_nomer.trim());
                                },2000);
                            }
                        } else {
                            $("#btdonew").text('Ошибка');
                        }
                    }
                });
            }
        }else{
           alert('Некорректно заполнены данные');
           clear = false
        }
    });
    $('#tcreg > tbody  > tr').each(function (i, el) {
        if (el['childNodes']['1']){
                Client_id = $(this).find(".tdclientid").html();
                tdtnetid = $(this).find(".tdtnetid").html();
        }
        $.ajax({
            type:'POST',
            url: '/mks_editor/post/get_clrezerv',
            dataType: 'json',
            data: {
                    csrfmiddlewaretoken: csrftoken,
                    data: Client_id,
            },
            beforeSend: function( xhr ) {
            },
            success: function(data_out){
                if (data_out){
                    data_out = data_out.split(',')
                    cl_id = data_out[0]
                    clrezerv = data_out[1]
                    Limit_Rez_Old = $('#tdlimitro_' + cl_id).html();
                    if (parseInt(Limit_Rez_Old) < parseInt(clrezerv)){
                        $('#tdlimitro_' + cl_id).addClass('red');
                    }
                   $('#tdclrezerv_' + cl_id).text(clrezerv)
                } else {
                    $("#btdonew").text('Ошибка');
                }
            }
        });
    });
    color_rt()


    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function color_rt(){
        tdtnetid_old = ""
        style_tr_old = "net1"
        $('#tcreg > tbody  > tr').each(function (i, el) {
            if (el['childNodes']['1']){
                Client_id = $(this).find(".tdclientid").html();
                tdtnetid = $(this).find(".tdtnetid").html();
            }
            if ((tdtnetid_old == tdtnetid) ||  i == 0){
                if (style_tr_old == "net1"){
                    $(this).find(".tdtnetid").parent().addClass('net1');
                    $(this).find(".tdlimit").addClass('net1');
                    style_tr_old = "net1"
                }else{
                    $(this).find(".tdtnetid").parent().addClass('net2');
                    $(this).find(".tdlimit").addClass('net2');
                    style_tr_old = "net2"
                }
            }
            else{
                if (style_tr_old == "net1"){
                    $(this).find(".tdtnetid").parent().addClass('net2');
                    $(this).find(".tdlimit").addClass('net2');
                    style_tr_old = "net2"
                }else{
                    $(this).find(".tdtnetid").parent().addClass('net1');
                    $(this).find(".tdlimit").addClass('net1');
                    style_tr_old = "net1"
                }
            }
            tdtnetid_old = tdtnetid
        });
    }
});
