$(document).ready(function() {
    var csrftoken = getCookie('csrftoken')

    $('.container_2 table').on('click', '.clientmailsend', function(){
        if (confirm('Отправить клиенту повторно регистрационные данные?')) {
            reg_nomer = $( this ).parent().parent().find('td:eq(0)').text();

            $.ajax({
                type:'POST',
                url: '/mks_editor/post/send_email_again',
                dataType: 'json',
                data: {
                        csrfmiddlewaretoken: csrftoken,
                        reg_nomer: reg_nomer,
                },
                beforeSend: function( xhr ) {
                },
                success: function(data){
                    mail_data.html("отправка")
                }
            });

        } else {
        }
    });

    $('.container_2 table').on('click', '.restorekey', function(){
        if (confirm('Отправить клиенту рег. номер и пароль?')) {
            reg_nomer = $( this ).parent().parent().find('td:eq(0)').text();

            $.ajax({
                type:'POST',
                url: '/mks_editor/post/send_email_restorekey',
                dataType: 'json',
                data: {
                        csrfmiddlewaretoken: csrftoken,
                        reg_nomer: reg_nomer,
                },
                beforeSend: function( xhr ) {
                },
                success: function(data){
                    alert("успешно отправлен запрос на восстановление")
                },
                error: function(data){
                    alert("ошибка")
                }
            });
        } else {
        }
    });

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

});