from django.conf import settings
from django.conf.urls import url
from django.templatetags.static import static

from . import views
from django_pdfkit import PDFView
from django.urls import path, reverse
from .views import (
    show_category
)
import mptt_urls

from .views import tagged, detail_view

app_name = 'handbook'

urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    url(r'^category/(?P<hierarchy>.+)/create_(?P<pdf>.+)/$', views.show_category, name='create_pdf'),
    url(r'^post/(?P<slug>.+)/$', detail_view, name="detail"),
    url(r'^tag/(?P<slug>.+)/$', tagged, name="tagged"),
    url(r'^category/(?P<hierarchy>.+)/$', views.show_category, name='category'),
    url('^private-media/(?P<path>.*)$', views.StorageView.as_view(), name='pm'),
]
