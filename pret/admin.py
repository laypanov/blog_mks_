from .models import General
from django.contrib import admin
from solo.admin import SingletonModelAdmin

admin.site.register(
    General,
    SingletonModelAdmin,
    fields=[
        'general_groups_admin',
        'general_groups',
    ]
)
