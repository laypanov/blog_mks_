import openpyxl
from django.shortcuts import render
from django.http import HttpResponse
from reportlab.graphics.barcode import code128
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from .utils import group_required


@group_required()
def upload_to_pdf(request):
    try:
        if request.method == 'POST' and request.FILES['myfile']:
            myfile = request.FILES['myfile']
            return create_bar_codes(myfile)
        else:
            return render(request, 'pret/main.html')
    except:
        return render(request, 'pret/main.html')


def create_bar_codes(file):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'
    wb = openpyxl.load_workbook(file)
    sheet = wb['Лист1']

    rows = sheet.max_row
    cols = sheet.max_column

    c = canvas.Canvas(response, pagesize=[163, 83])  # 57x29

    for i in range(1, rows + 1):
        string = ''
        for j in range(1, cols + 1):
            cell = sheet.cell(row=i, column=j)
            string = string + str(cell.value) + ' '
            name = cell.value
        shk = sheet.cell(row=i, column=j - 1).value

        barcode_value = str(shk)
        barcode128 = code128.Code128(barcode_value, barHeight=20, stop=1, barWidth=2)

        x = 0
        y = 60

        barcode128.drawOn(c, x - 5, y + 2)
        c.setFont("Helvetica", 8)
        c.drawString(x + 70, y - 6, barcode_value)

        pdfmetrics.registerFont(TTFont('DejaVuSans', 'DejaVuSans.ttf'))
        c.setFont('DejaVuSans', 7)

        y = y - 20
        for x in range(0, len(name), 30):
            c.drawCentredString(80, y, name[x:x + 30])
            y = y - 10
        c.showPage()
    c.save()
    return response
