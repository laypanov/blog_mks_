import six
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import user_passes_test
from .models import General


def group_required(view=None, group=None, login_url=None, raise_exception=False):
    def check_perms(user):
        try:
            group = General.objects.get(id=1)
            # общая группа доступа
            general_groups = group.general_groups
            if isinstance(general_groups, six.string_types):
                groups = (general_groups,)
            else:
                groups = general_groups
        except:
            return False

        if user.is_authenticated:
            if user.groups.filter(name=groups).exists():
                return True
        else:
            if str(general_groups) == 'Служебный_Все':
                return True

        if raise_exception:
            raise PermissionDenied
        return False

    return user_passes_test(check_perms)
