from django.conf import settings
from django.conf.urls import url

from django.urls import path
from . import views

app_name = 'guard_monitor_cv'


urlpatterns = [
    url(r'^$', views.get_categories),
    url(r'^get_number_auto/$', views.get_number_auto),
]

