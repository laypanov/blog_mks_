import os
import random
import subprocess
import sys

import cv2
from django.db.models import Q
from django.shortcuts import render

from .conn_pass import *

from blog_mks import settings
from .models import Auto
from .utils import group_required
from datetime import datetime, timedelta, time


@group_required()
def get_categories(request):
    today = datetime.now().date()
    tomorrow = today + timedelta(1)
    today_start = datetime.combine(today, time())
    today_end = datetime.combine(tomorrow, time())
    auto = Auto.objects.filter(date__date=today_start).order_by('-date')
    context = {
        'auto': auto,
        'date__lte': today_start,
        'date__gte': today_start,
    }
    return render(request, "guard_monitor_cv/categories.html", context)


@group_required()
def get_pic_range(request):
    if request.user.is_authenticated:
        if request.is_ajax and request.method == "GET":
            date__lte = request.GET.get('date__lte')
            date__gte = request.GET.get('date__gte')
            date__lte = datetime.strptime(date__lte, '%d.%m.%Y')
            date__gte = datetime.strptime(date__gte, '%d.%m.%Y')
    auto = Auto.objects.filter(Q(date__range=[date__gte, date__lte])
                               | Q(date__date=date__lte) | Q(date__date=date__gte)).order_by('-date')

    context = {
        'auto': auto,
        'date__lte': date__lte,
        'date__gte': date__gte,
    }
    return render(request, "guard_monitor_cv/categories.html", context)


@group_required()
def get_number_auto(self):
    global t

    import difflib

    from other_projects.nomeroffnet.NomeroffNet.YoloV5Detector import Detector
    from other_projects.nomeroffnet.NomeroffNet.BBoxNpPoints import (NpPointsCraft,
                                                                     getCvZoneRGB,
                                                                     convertCvZonesRGBtoBGR,
                                                                     reshapePoints)
    from other_projects.nomeroffnet.NomeroffNet.OptionsDetector import OptionsDetector
    from other_projects.nomeroffnet.NomeroffNet.TextDetector import TextDetector
    from other_projects.nomeroffnet.NomeroffNet.TextPostprocessing import textPostprocessing

    NOMEROFF_NET_DIR = os.path.abspath('../other_projects/nomeroffnet')
    sys.path.append(NOMEROFF_NET_DIR)

    os.environ["CUDA_VISIBLE_DEVICES"] = ""
    os.environ["TF_FORCE_GPU_ALLOW_GROWTH"] = "true"

    dirname = settings.MEDIA_ROOT + '/guard_monitor_cv/jpgs/'
    dirname_tmp = settings.MEDIA_ROOT + '/tmp/guard_monitor_cv/'

    cam_ip = ''

    global last_textArr
    last_textArr = []
    global p
    p = []

    detector = Detector()
    detector.load()
    npPointsCraft = NpPointsCraft()
    npPointsCraft.load()

    def get_frame_ip(frames=2):
        for i in range(frames):
            with open(os.devnull, 'wb') as devnull:
                url1 = 'http://{0}:{1}@{2}/cgi-bin/snapshot.cgi?1'.format(cam_login, cam_pass, cam_ip)
                name = "rec_frame_%s.jpg" % random.random()
                path = os.path.join(dirname_tmp, name)
                subprocess.call("wget -O %s %s" % (path, url1), stdout=devnull, shell=True, stderr=subprocess.STDOUT)
                p.append(path)

    from threading import Timer

    class perpetualTimer():
        def __init__(self, t, hFunction):
            self.t = t
            self.hFunction = hFunction
            self.thread = Timer(self.t, self.handle_function)

        def handle_function(self):
            self.hFunction()
            self.thread = Timer(self.t, self.handle_function)
            self.thread.start()

        def start(self):
            self.thread.start()

        def cancel(self):
            self.thread.cancel()

    def check():
        try:
            now = datetime.now()
            dt_string_now = now.strftime("%d.%m.%Y %H:%M:%S")
            dt_string = now.strftime("%d_%m_%Y")
            dt_h = now.strftime("%H")
            dt_m = now.strftime("%M")
            dt_s = now.strftime("%S")

            get_frame_ip()

            for path in p:

                obj = Auto.objects.filter().order_by('-id')[0]
                group = None
                cl = False
                img_path = path
                img = cv2.imread(img_path)
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

                targetBoxes = detector.detect_bbox(img)
                all_points = npPointsCraft.detect(img,
                                                  targetBoxes,
                                                  [5, 2, 0])

                zones = convertCvZonesRGBtoBGR([getCvZoneRGB(img, reshapePoints(rect, 1)) for rect in all_points])

                optionsDetector = OptionsDetector()
                optionsDetector.load("latest")

                textDetector = TextDetector.get_static_module("ru")()
                textDetector.load("latest")

                regionIds, countLines = optionsDetector.predict(zones)
                regionNames = optionsDetector.getRegionLabels(regionIds)

                textArr = textDetector.predict(zones)
                textArr = textPostprocessing(textArr, regionNames)

                if int(dt_m) % 2 == 0 and dt_s in range(10):
                    last_textArr.clear()

                for lta in last_textArr:
                    if textArr == lta and lta != '[]':
                        os.remove(path)
                        cl = True
                if cl is False and textArr != '[]':
                    last_textArr.append(textArr)

                    def similarity(s1, s2):
                        normalized1 = s1.lower()
                        normalized2 = s2.lower()
                        matcher = difflib.SequenceMatcher(None, normalized1, normalized2)
                        return matcher.ratio()

                    s = None
                    try:
                        obj = Auto.objects.filter().order_by('-id')[0]
                    except:
                        group = 0

                    old_file = path
                    pi = '[%s]_' % random.random() + '_'.join(map(str, textArr))
                    if not os.path.exists(dirname + '{0}'.format(dt_string)):
                        print(dirname + '{0}'.format(dt_string))
                        os.makedirs(dirname + '{0}'.format(dt_string))
                    new_file = os.path.join(
                        dirname + '{0}/{1}.jpg'.format(dt_string, pi))
                    os.rename(old_file, new_file)
                    for number in textArr:
                        if group is None:
                            group = obj.group + 1

                        for l1 in last_textArr[-1:]:
                            s = similarity(l1[0], number)
                            print('l1-->', l1[0], '[s=]', s, ' number-->', number, dt_string_now)
                            obj = Auto.objects.filter().order_by('-id')[0]
                            if float(s) < float(0.5):
                                group = obj.group + 1
                            elif obj.number != number:
                                group = obj.group + 1
                            else:
                                group = obj.group

                            query = Auto(
                                number=number,
                                group=group,
                                image='guard_monitor_cv/jpgs/{0}/{1}.jpg'.format(dt_string, pi),
                            )
                            query.save()
                cv2.destroyAllWindows()
                p.remove(path)
        except Exception as e:
            now = datetime.now()
            dt_string_now = now.strftime("%d.%m.%Y %H:%M:%S")
            p.remove(path)
            os.remove(path)
            print('except -> reload -> ', dt_string_now, e)

    try:
        t = perpetualTimer(1, check)
        t.start()
    except:
        now = datetime.now()
        dt_string_now = now.strftime("%d.%m.%Y %H:%M:%S")
        print('except2 -> reload -> ', dt_string_now)
        t.cancel()
        t = perpetualTimer(1, check)
        t.start()
