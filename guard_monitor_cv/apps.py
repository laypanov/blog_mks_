from django.apps import AppConfig


class GuardMonitorCvConfig(AppConfig):
    name = 'guard_monitor_cv'
    verbose_name = 'Мониторинг автомобилей'
