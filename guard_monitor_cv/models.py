from django.contrib.auth.models import Group
from django.db import models


class Auto(models.Model):
    number = models.CharField('Номер автомобиля', max_length=255)
    group = models.IntegerField('Группа')
    image = models.ImageField('Изображение', max_length=255)
    date = models.DateTimeField('Дата', auto_now_add=True)

    class Meta:
        ordering = ('number',)
        verbose_name = 'Список номеров'
        verbose_name_plural = 'список номеров'
        app_label = 'guard_monitor_cv'

    def __str__(self):
        return self.number


class General(models.Model):
    general_groups_admin = models.ForeignKey(Group, related_name='guard_monitor_cv_general_groups_admin',
                                             verbose_name='Администратор группы',
                                             on_delete=models.CASCADE, null=True)

    general_groups = models.ForeignKey(Group, related_name='guard_monitor_cv_general_groups',
                                       verbose_name='Разрешение для группы',
                                       on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = 'Группа доступа'
        verbose_name_plural = 'группа доступа'

    def __str__(self):
        return str(self.general_groups)
