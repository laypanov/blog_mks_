from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import Auto, General


class AutoInstanceAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': (
                'number',
                'group',
                'image',
                'date',
            )
        }),
    )
    list_display = [
        'number',
        'group',
        'date',
    ]

    readonly_fields = ['date']


admin.site.register(Auto, AutoInstanceAdmin)

admin.site.register(
    General,
    SingletonModelAdmin,
    fields=[
        'general_groups_admin',
        'general_groups',
    ]
)

