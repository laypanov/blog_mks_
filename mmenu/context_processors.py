import six

from account.models import EXTLynxDB
from hr.models import UOlData, General, UOlDataAdvSign
from mmenu.models import MMenu, MMenuItems
from django.contrib.auth.models import User, Group

# вернем список групп в которые включен пользователь, Все - есть все
from users_info.models import UsersExtLynxEmploys


def group_required_post(request):
    username = str(request.user.username)
    try:
        usr = User.objects.get(username=username)
        groups = [x.name for x in usr.groups.all()]
    except:
        return ['Служебный_Все', 'Служебный_Инструкции']

    # временно для доступа в ОХ
    username = str(request.user.username)
    user = User.objects.get(username=username)
    ext_user = EXTLynxDB.objects.get(user=user)
    author = UsersExtLynxEmploys.objects.get(employ_id=ext_user.ldb_employid)
    sign = UOlDataAdvSign.objects.filter(author=author)
    sign = sign.all()

    if sign:
        groups.append("Служебный_Обходной_лист")

    return groups


def mmenu_items(request):
    # выводим только доступные для пользователя меню
    creators_list = group_required_post(request)
    mmenu_items = MMenuItems.objects.filter(general_groups__name__in=creators_list)
    return {'mmenu_items': mmenu_items}


def mmenu(request):
    return {'mmenu': MMenu.objects.filter(title='Основное меню')}
