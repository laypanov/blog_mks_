from django.contrib import admin
from mmenu.models import MMenuItems, MMenu
from imagekit.admin import AdminThumbnail
from solo.admin import SingletonModelAdmin


class MMenuItemInline(admin.TabularInline):
    model = MMenuItems
    fields = ['title', 'url', 'pic_bp', 'position', 'general_groups']
    list_display = ['title',
                    'position']


class MMenuAdmin(SingletonModelAdmin):
    fields = ['title', 'title_brand', 'image_brand']
    admin_thumbnail = AdminThumbnail(image_field='image_brand_thumbnail')
    list_display = ['title', 'title_brand', 'admin_thumbnail']
    search_fields = ['title']
    inlines = [MMenuItemInline]


admin.site.register(MMenu, MMenuAdmin)
