from django.db import models
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit
from django.contrib.auth.models import Group


class MMenu(models.Model):
    title = models.CharField(max_length=50,
                             verbose_name='Название',
                             default='Основное меню',
                             # editable=False,
                             unique=True
                             )
    title_brand = models.CharField(max_length=50,
                                   verbose_name='Заголовок бренда'
                                   )
    image_brand = models.ImageField(upload_to='brand')
    image_brand_thumbnail = ImageSpecField(source='image_brand',
                                           processors=[ResizeToFit(100, 100),
                                                       ],
                                           format='JPEG',
                                           options={'quality': 90})

    def thumb_img(self):
        if self.image_brand:
            return u'<img src="%s" width="100"/>' % self.image_brand_thumbnail.url
        else:
            return '(none)'

    thumb_img.short_description = 'Thumb'
    thumb_img.allow_tags = True

    class Meta:
        ordering = ('title',)
        verbose_name = 'основное меню'
        verbose_name_plural = 'Основное'

    def __str__(self):
        return self.title


class MMenuItems(models.Model):
    url = models.CharField(max_length=255)
    # parent = models.ForeignKey(Person, verbose_name='parent', null=True, blank=True, on_delete=models.PROTECT)
    menu = models.ForeignKey(MMenu, on_delete=models.PROTECT,
                             verbose_name='Привязка к основному меню',
                             )
    title = models.CharField(max_length=50,
                             verbose_name='Название',
                             )
    pic_bp = models.CharField(max_length=50,
                              verbose_name='Название иконки bootstrap',
                              )
    position = models.IntegerField(verbose_name='Позиция в списке',
                                   unique=True)

    general_groups = models.ForeignKey(Group, related_name='mmenu_general_groups', verbose_name='Разрешение для группы',
                                       on_delete=models.CASCADE, null=True)

    class Meta:
        ordering = ('position',)
        verbose_name = 'Категории меню'
        verbose_name_plural = 'Категории меню'

    def __str__(self):
        return self.title
