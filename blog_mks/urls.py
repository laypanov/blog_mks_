import mptt_urls
from django.conf.urls import include, url
from django.views.static import serve
from django.conf import settings
from account.views import LoginView
from blog_mks import views
from blog_mks.views import h404
from pret.views import upload_to_pdf
from sklad_kpi import views as sklad_kpi_views
from search.views import ESearchView
from tt_informations import views as tt_informations_views
from hr import views as hr_views
from mks_editor import views as mks_editor
from services import views as services
from guard_monitor_cv import views as guard_monitor_cv_views
from tablet_updates import views as tablet_updates_views
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import path
import private_storage.urls


def get_filename(filename, request):
    return filename.upper()


urlpatterns = [
    path(r'', include('landing_page.urls', namespace='main')),

    path('search/', ESearchView.as_view(), name='search'),
    url(r'^attachments/', include('attachments.urls', namespace='attachments')),
    path(r'handler404/', h404),
    path(r'admin/login/', LoginView.as_view()),
    path(r'mks/', include('mks_data.urls', namespace='mks_data')),
    path('ckeditor_uploader/', include('ckeditor_uploader.urls')),
    path(r'mks_editor/', include('mks_editor.urls', namespace='mks_editor')),
    path(r'services/', include('services.urls', namespace='services')),
    path(r'access_blank/', include('access_blank.urls', namespace='access_blank')),
    path(r'handbook/', include('handbook.urls'), name='handbook'),
    path(r'regulations/', include('regulations.urls'), name='regulations'),
    path(r'accounts/', include('account.urls')),
    path(r'users_info/', include('users_info.urls')),
    url(r'hr/', include('hr.urls')),
    url(r'guard_monitor_cv/', include('guard_monitor_cv.urls')),
    path(r'tt_informations/', include('tt_informations.urls'), name='tt_informations'),

    url(r'^mks_editor/new/$', mks_editor.mks_editor_new, name='mks_editor_new'),
    url(r'^mks_editor/details/(?P<slug>.+)/$', mks_editor.details, name="mks_editor_details"),

    url(r'^guard_monitor_cv/get_pic_range/$', guard_monitor_cv_views.get_pic_range, name='get_pic_range'),

    url(r'^hr/ol/$', hr_views.config, name='hr_ol'),
    url(r'^hr/ol/config/$', hr_views.config, name='hr_config'),
    url(r'^hr/ol/new/$', hr_views.hr_ol_new, name='hr_ol_new'),
    url(r'^hr/ol/new/get_upodr/$', hr_views.hr_ol_new_get_upodr, name='hr_ol_new_get_upodr'),
    url(r'^hr/ol/new/get_date_of_end/$', hr_views.get_date_of_end, name='get_date_of_end'),
    url(r'^hr/ol/new/create_new/$', hr_views.create_new, name='create_new'),
    url(r'^hr/ol/config/upodr_del/$', hr_views.hr_ol_upodr_del, name='hr_ol_upodr_del'),
    url(r'^hr/ol/config/podr_del/$', hr_views.hr_ol_podr_del, name='hr_ol_podr_del'),
    url(r'^hr/ol/config/upodr/$', hr_views.hr_ol_upodr, name='hr_ol_upodr'),

    url(r'^tt_informations/export/xls/', tt_informations_views.export_xls, name='tt_informations_export_xls'),
    url(r'^tt_informations/maps', tt_informations_views.maps, name='tt_informations_maps'),
    url(r'^tt_informations/question/new/', tt_informations_views.new, name='tt_informations_new'),
    url(r'^tt_informations/$', tt_informations_views.questionnaires_list, name='tt_informations_ql'),
    url(r'^tt_informations/question/(?P<slug>.+)/$', tt_informations_views.question_detail, name="question_detail"),

    url(r'^tablet_updates/get_info/$', tablet_updates_views.get_info, name='gi'),

    path(r'pret/', upload_to_pdf),

    path(r'sklad_kpi/', sklad_kpi_views.view_users_kpi),
    url(r'^sklad_kpi/simple/', sklad_kpi_views.view_users_kpi_simple, name='view_users_kpi_simple'),

    url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),
    url(r'^taggit/', include('taggit_selectize.urls')),
    url(r'^handbook/category/(?P<path>.*)', mptt_urls.view(model='handbook.models.Category',
                                                           view='handbook.views.show_category', slug_field='slug',
                                                           trailing_slash=True),
        {'extra': ''}, name='handbook'),
    url(r'^regulations/category/(?P<path>.*)', mptt_urls.view(model='regulations.models.Category',
                                                              view='regulations.views.show_category', slug_field='slug',
                                                              trailing_slash=True),
        {'extra': ''}, name='regulations'),
]

urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),
)

if settings.DEBUG:
    urlpatterns += [
        url(r'media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]
