from django.contrib.auth.models import User


def get_filename(filename):
    return filename.upper()


def group_required_post(request):
    groups = []
    if request.user.is_authenticated:
        username = str(request.user.username)
        try:
            usr = User.objects.get(username=username)
            groups = [x.name for x in usr.groups.all()]
        except:
            pass
    groups.append('Служебный_Все')
    return groups
