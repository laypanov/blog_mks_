from blog_mks import settings


class AnalitikattRouter(object):
    default_apps = (
        'admin',
        'auth',
        'contenttypes',
        'sessions',
        'messages',
    )

    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'tt_informations':
            return 'tt_informations'
        return 'default'

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'tt_informations':
            return 'tt_informations'
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        db_list = 'tt_informations'
        if obj1._state.db in db_list and obj2._state.db in db_list:
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'tt_informations':
            return 'tt_informations'
        return 'default'
