from django.shortcuts import redirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.views import generic
from . import forms


@login_required
def main(request):
    return redirect('main:landing_page')


class CkEditorFormView(generic.FormView):
    form_class = forms.CkEditorForm
    template_name = "form.html"

    def get_success_url(self):
        return reverse("ckeditor-form")


ckeditor_form_view = CkEditorFormView.as_view()


def h404(request):
    return render(request, 'handler404.html')

