CRONJOBS = [
    ('00 9 * * *', 'tt_informations.views.send_email_analitik'),
    ('*/30 * * * *', 'users_info.views.update_db'),
    ('*/40 * * * *', 'users_info.views.sync_db'),
    ('*/5 * * * *', 'hr.views.send_email_date_write'),
    ('*/5 * * * *', 'hr.views.send_email_new'),
    ('*/5 * * * *', 'hr.views.send_email_date_write_adv'),
    ('*/5 * * * *', 'hr.views.send_email_date_write_lastday()'),
    # ('*/1 * * * *', 'mks_editor.views.send_email_regdata_day'),
]
