from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required


@login_required
def landing_page(request):
    return render(request, 'landing_page/index.html')

