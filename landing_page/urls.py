from django.conf.urls import url
from landing_page import views
from django.conf import settings
app_name = 'landing_page'

urlpatterns = [
    url(r'^$', views.landing_page, name='landing_page'),
]

