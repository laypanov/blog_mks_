from django import forms
from django.forms import TextInput, ChoiceField, TypedChoiceField, Select, IntegerField, NumberInput, ModelForm, \
    DateTimeField

from .models import Podr, UPodr, UOlData


class PostFormPodr(forms.ModelForm):
    class Meta:
        model = Podr
        fields = (
            'podr',
        )
        widgets = {
            "podr": TextInput(attrs={'class': 'vTextField'}),
        }
        labels = {
            'podr': 'Название нового подразделения:',
        }


class PostFormPodrSelect(forms.ModelForm):
    class Meta:
        model = Podr
        fields = (
            'podr',
        )
        widgets = {
            "podr": Select(attrs={}),
        }
        labels = {
            'podr': 'Подразделение:',
        }


class PostFormUPodr(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PostFormUPodr, self).__init__(*args, **kwargs)
        self.fields['employ_id'].queryset = self.fields['employ_id'].queryset.order_by("family").filter(active=1)
        self.fields['podr'].queryset = self.fields['podr'].queryset.order_by("podr")

    class Meta:
        model = UPodr
        fields = (
            'employ_id',
            'podr',
        )
        widgets = {
            "podr": Select(attrs={}),
            "employ_id": Select(attrs={}),
        }
        labels = {
            'podr': 'Подразделение:',
            'employ_id': 'Сотрудник:',
        }


class DateInput(forms.DateInput):
    input_type = 'date'


class PostFormUOlData(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PostFormUOlData, self).__init__(*args, **kwargs)
        self.fields['employ_id'].queryset = self.fields['employ_id'].queryset.filter(active=1).order_by("family")

    class Meta:
        model = UOlData
        fields = (
            'employ_id',
            'date_dism',
        )
        widgets = {
            'date_dism': DateInput(attrs={})
        }
        labels = {
            'employ_id': 'Сотрудник',
            'date_dism': 'Дата увольнения',
        }
