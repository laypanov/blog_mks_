from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import UPodr, UOlData, UOlDataAdvComments, UOlDataAdvSign, Podr, General


class UPodrInstanceAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': (
                'podr',
                'employ_id',
            )
        }),
    )
    list_display = [
        'podr',
        'employ_id',
    ]


admin.site.register(UPodr, UPodrInstanceAdmin)


class UOlDataInstanceAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': (
                'employ_id',
                'date_create',
                'date_write',
                'date_dism',
                'date_close',
                'status',
                'write_email',
                'write_email_adv',
                'write_email_lastday',
            )
        }),
    )
    list_display = [
        'employ_id',
        'date_create',
        'date_write',
        'date_dism',
        'date_close',
        'status',
        'write_email',
        'write_email_adv',
        'write_email_lastday',
    ]
    readonly_fields = ['date_create']


admin.site.register(UOlData, UOlDataInstanceAdmin)


class UOlDataAdvCommentsInstanceAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': (
                'author',
                'uoldata',
                'comment',
                'date_create',
            )
        }),
    )
    list_display = [
        'author',
        'uoldata',
        'comment',
        'date_create',
    ]
    readonly_fields = ['date_create']


admin.site.register(UOlDataAdvComments, UOlDataAdvCommentsInstanceAdmin)


class UOlDataAdvSignInstanceAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': (
                'author',
                'uoldata',
                'date_sign',
                'status',
                'new_email_write',
            )
        }),
    )
    list_display = [
        'author',
        'uoldata',
        'date_sign',
        'status',
        'new_email_write',
    ]


admin.site.register(UOlDataAdvSign, UOlDataAdvSignInstanceAdmin)


class PodrInstanceAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': (
                'podr',
            )
        }),
    )
    list_display = [
        'id',
        'podr',
    ]


admin.site.register(Podr, PodrInstanceAdmin)

admin.site.register(
    General,
    SingletonModelAdmin,
    fields=[
        'general_groups_admin',
        'hr_ol_general_groups_write_email',
        'general_groups',
    ]
)
