import json
import smtplib
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import date

import pyodbc
import six
from django.contrib.auth import models
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect

from account.models import EXTLynxDB
from hr.forms import PostFormPodr, PostFormUPodr, PostFormUOlData
from hr.models import UPodr, UOlData, Podr, UOlDataAdvSign, General, UOlDataAdvComments
from users_info.conn_pass import lynx_user_admin, lynx_password_admin
from users_info.models import UsersExtLynxEmploys, UsersExtLynxUsers, UsersExtAD
from .utils import group_required_admin


def ol(request):
    perm = False
    if request.user.is_authenticated:
        uoldata = UOlData.objects.all()
        username = str(request.user.username)
        user = User.objects.get(username=username)

        # если есть полные права, показываем все ОЛ
        try:
            group = str(General.objects.filter(id=1)[0])
            if isinstance(group, six.string_types):
                groups = (group,)
            else:
                groups = group
        except:
            pass
        if user.groups.filter(name__in=groups).exists():
            perm = True

        if perm:
            sign = UOlDataAdvSign.objects.all().order_by('-uoldata')
        else:
            sign = group_view(request).order_by('-uoldata')
    else:
        return HttpResponseRedirect("/accounts/login/?next=/hr/ol/")

    return render(request, "hr/ol/categories.html", {'uoldata': uoldata, 'sign': sign})


@group_required_admin()
def hr_ol_new(request):
    # сотрудник
    form_uoldata = PostFormUOlData()
    # подписывающие
    form_upodr = PostFormUPodr()

    return render(request, "hr/ol/new.html", {'form_uoldata': form_uoldata, 'form_upodr': form_upodr})


@group_required_admin()
def hr_ol_new_get_upodr(request):
    if request.is_ajax and request.method == "POST":
        podr = request.POST.get('podr')
        upodr_out = []
        upodr = UPodr.objects.filter(podr__podr=podr)
        for p in upodr.all():
            upodr_out.append(str(p.employ_id))
        return HttpResponse(json.dumps(upodr_out), content_type="application/json;charset=utf-8")


@group_required_admin()
def get_date_of_end(request):
    if request.is_ajax and request.method == "POST":
        employ_id = request.POST.get('employ_id').split('[')
        employ_id = employ_id[1].split(']')
        employ_id = employ_id[0]
        date_of_end = UsersExtLynxEmploys.objects.get(employ_id=employ_id)
        if date_of_end.date_of_end:
            return HttpResponse(json.dumps(str(date_of_end.date_of_end)), content_type="application/json;charset=utf-8")
        else:
            username = lynx_user_admin
            password = lynx_password_admin

            cursor = cnxn.cursor()
            cursor.execute("""SELECT [DateOfEnd] FROM [ViewEmploys] where Employ_ID=%s""" % employ_id)

            date = cursor.fetchone()
            if date[0]:

                return HttpResponse(json.dumps(str(date[0])), content_type="application/json;charset=utf-8")
            else:
                return HttpResponse(json.dumps(str("None")), content_type="application/json;charset=utf-8")


@group_required_admin()
def create_new(request):
    if request.is_ajax and request.method == "POST":
        try:
            employ_id = request.POST.get('employ').split('[')
            employ_id = employ_id[1].split(']')
            employ_id = employ_id[0]
            sing_users = request.POST.get('sing_users')
            date_dism = request.POST.get('date_dism')

            employs = UsersExtLynxEmploys.objects.get(employ_id=employ_id)
            UOlData.objects.update_or_create(
                employ_id=employs,
                defaults={'date_dism': date_dism, 'status': False}
            )
            json_query = json.loads(sing_users)
            uoldata = UOlData.objects.get(employ_id__employ_id=employ_id)

            for j in json_query:
                j = j.split('[')
                j = j[1].split(']')
                j = j[0]

                author = UsersExtLynxEmploys.objects.get(employ_id=j)
                UOlDataAdvSign.objects.update_or_create(
                    author=author,
                    uoldata=uoldata,
                    defaults={'status': False}
                )
            return HttpResponse(json.dumps(str(uoldata.id)), content_type="application/json;charset=utf-8")
        except:
            return HttpResponse(json.dumps(str("None")), content_type="application/json;charset=utf-8")


@group_required_admin()
def close_ol(request, slug):
    if request.is_ajax and request.method == "POST":
        UOlData.objects.update_or_create(
            id=slug,
            defaults={'date_close': datetime.now(), 'status': True}
        )
        return HttpResponseRedirect("/hr/ol/details/%s/" % slug)


def add_comment(request, slug):
    if request.is_ajax and request.method == "POST":
        user = request.POST.get('user')
        comment = request.POST.get('comment')
        if comment and user:
            author = UsersExtLynxUsers.objects.get(user_name__icontains=user)
            uoldata = UOlData.objects.get(id=slug)
            UOlDataAdvComments.objects.create(author=author.employ_id, uoldata=uoldata, comment=comment)
        return HttpResponseRedirect("/hr/ol/details/%s/" % slug)


def details(request, slug):
    try:
        username = str(request.user.username)
        user = User.objects.get(username=username)
        ext_user = EXTLynxDB.objects.get(user=user)
    except:
        return HttpResponseRedirect("/accounts/login/?next=/hr/ol/details/%s/" % slug)

    if request.user.is_authenticated:
        if request.is_ajax and request.method == "POST":
            uoldata = request.POST.get('uoldata')
            UOlData.objects.filter(id=uoldata)
            author = UsersExtLynxEmploys.objects.get(employ_id=ext_user.ldb_employid)
            UOlDataAdvSign.objects.update_or_create(
                author=author,
                uoldata=uoldata,
                defaults={'status': True, 'date_sign': datetime.now()}
            )
            uoldata = UOlData.objects.get(id=slug)
            d = UOlDataAdvSign.objects.filter(uoldata=uoldata)
            if uoldata.date_write is None and d.count() == d.filter(status=True).count():
                UOlData.objects.update_or_create(
                    employ_id=uoldata.employ_id,
                    defaults={'date_write': datetime.now()}
                )

        perm = False
        try:
            uoldata = UOlData.objects.get(id=slug)
        except:
            return redirect('hr_ol')

        try:
            group = str(General.objects.filter(id=1)[0])
            if isinstance(group, six.string_types):
                groups = (group,)
            else:
                groups = group
        except:
            pass

        if user.groups.filter(name__in=groups).exists():
            perm = True

        for s in uoldata.uoldataadvsign_set.all():
            try:
                if s.author.employ_id == ext_user.ldb_employid:
                    perm = True
            except:
                pass

        if perm:
            try:
                # подписан ли уже документ от пользователя под которым вошли
                sign = uoldata.uoldataadvsign_set.get(author__employ_id=ext_user.ldb_employid)
                s_status = sign.status
            except Exception as e:
                print(e)
                s_status = True

            # рассчитываем % готовности
            sign = uoldata.uoldataadvsign_set.all()
            sign_count = sign.all().count()
            sign_done = sign.filter(status=True).count()
            sign_proc = sign_done * 100 / sign_count
            if sign_proc == 0:
                sign_proc = 5

            # является ли юзер админом ОЛ
            g = General.objects.get()
            group = models.Group.objects.get(name=g.general_groups_admin)
            users = group.user_set.all()
            user_admin = None
            for user in users:
                if str(user) == str(username) and uoldata.status is False:
                    user_admin = 1

            username = lynx_user_admin
            password = lynx_password_admin

            cursor = cnxn.cursor()

            cursor.execute("""Exec dbo.Os_CardLst "0R",1,0""")
            count_os = 0
            for row in cursor.fetchall():
                if row.NameMOL is not None:
                    if row.NameMOL.strip() == uoldata.employ_id.name or row.NameMOL.strip() == "{0}{1}{2}".format(
                            uoldata.employ_id.family,
                            uoldata.employ_id.first_name, uoldata.employ_id.second_name):
                        count_os = count_os + 1

            return render(request, "hr/ol/details.html",
                          {'user_admin': user_admin, 'uoldata': uoldata, 's_status': s_status, 'count_os': count_os,
                           'sign_proc': ("%.0f" % sign_proc)})
        else:
            return redirect('hr_ol')
    else:
        return HttpResponseRedirect("/accounts/login/?next=/hr/ol/details/%s/" % slug)


@group_required_admin()
def config(request):
    if request.method == "POST":
        form = PostFormPodr(request.POST)
        if form.is_valid():
            form.save()
            return redirect('hr_config')
        # нужно показать ошибку
        else:
            return redirect('hr_config')
    else:
        form = PostFormPodr()
        form_upodr = PostFormUPodr()
        upodr = UPodr.objects.all().order_by('podr')
        # ну ваще никак по нормальному(
        p2 = []
        for p in upodr.all():
            p2.append(p)
        podr = Podr.objects.exclude(podr__in=p2)
        return render(request, "hr/ol/config.html",
                      {'form': form, 'form_upodr': form_upodr, 'upodr': upodr, 'podr': podr})


@group_required_admin()
def hr_ol_upodr(request):
    if request.method == "POST":
        form_upodr = PostFormUPodr(request.POST)
        if form_upodr.is_valid():
            form_upodr.save()
            return redirect('hr_config')
        else:
            return redirect('hr_config')
    return redirect('hr_config')


@group_required_admin()
def hr_ol_upodr_del(request):
    if request.is_ajax and request.method == "POST":
        podr = request.POST.get('podr')
        employ_id = request.POST.get('employ_id')
        csrfmiddlewaretoken = request.POST.get('csrfmiddlewaretoken')

        if csrfmiddlewaretoken and podr and employ_id:
            employ_id = employ_id.split('[')
            employ_id = employ_id[1].split(']')
            UPodr.objects.get(podr__podr=podr, employ_id=employ_id[0]).delete()
    return redirect('hr_config')


@group_required_admin()
def hr_ol_podr_del(request):
    if request.is_ajax and request.method == "POST":
        podr = request.POST.get('podr')
        csrfmiddlewaretoken = request.POST.get('csrfmiddlewaretoken')

        if csrfmiddlewaretoken and podr:
            Podr.objects.get(podr=podr).delete()
    return redirect('hr_config')


# вернем список ОЛ в которые включен пользователь
def group_view(request):
    try:
        username = str(request.user.username)
        user = User.objects.get(username=username)
        ext_user = EXTLynxDB.objects.get(user=user)
        author = UsersExtLynxEmploys.objects.get(employ_id=ext_user.ldb_employid)
        sign = UOlDataAdvSign.objects.filter(author=author)
        sign = sign.all()
    except:
        pass
    return sign


def send_email_new():
    uoldataadvsign = UOlDataAdvSign.objects.filter(Q(new_email_write=None) | Q(new_email_write=False))
    mails_to = []

    def mail(mails):
        try:
            msg = MIMEMultipart()
            server = smtplib.SMTP('')
            toaddr = mails
            fromaddr = ''
            rcpt = [toaddr] + [fromaddr]
            msg['to'] = toaddr
            msg['Subject'] = 'Обходной лист [{0}, увольнение: {1}]'.format(uolsign.uoldata.employ_id.name,
                                                                           uolsign.uoldata.date_dism)
            body = "<a href=http://url/hr/ol/details/{0}/>Вы участвуете в подписании</a>" \
                .format(uolsign.uoldata.id)
            msg.attach(MIMEText(body, 'html', _charset='utf-8'))
            text = msg.as_string()
            server.sendmail(fromaddr, rcpt, text)
        except:
            return False
        return True

    for uolsign in uoldataadvsign:
        uead = UsersExtLynxEmploys.objects.get(employ_id=uolsign.author.employ_id)
        ad = UsersExtAD.objects.get(employ_id_id=uead)

        if mail(ad.email):
            UOlDataAdvSign.objects.filter(uoldata__employ_id=uolsign.uoldata.employ_id,
                                          author__employ_id=uolsign.author.employ_id).update(new_email_write=True)


# отправка почты с уведомлением о полном подписании для групп hr_ol_general_groups_write_email
def send_email_date_write():
    # полностью пописан и не закрыт
    uoldata = UOlData.objects.filter(status=False, date_write__isnull=False).filter(
        Q(write_email=None) | Q(write_email=False))
    mails_to = []
    for u in uoldata:
        # какие пользователи состоят в группе
        g = General.objects.get()
        group = models.Group.objects.get(name=g.hr_ol_general_groups_write_email)
        users = group.user_set.all()
        # получаем майлы этию юзеров
        for us in users:
            try:
                ext_user = EXTLynxDB.objects.get(user=us)
                uead = UsersExtLynxEmploys.objects.get(employ_id=ext_user.ldb_employid)
                ad = UsersExtAD.objects.get(employ_id_id=uead)
                mails_to.append(ad.email)
            except:
                pass

        def mail(mails):
            try:
                msg = MIMEMultipart()
                server = smtplib.SMTP('')
                toaddr = mails
                fromaddr = ''
                msg['Subject'] = 'Обходной лист [{0}, увольнение: {1}]'.format(u.employ_id.name, u.date_dism)
                body = "<a href=http://url/hr/ol/details/{0}/>" \
                       "Подписан всеми участниками</a>".format(u.id)
                msg.attach(MIMEText(body, 'html', _charset='utf-8'))
                text = msg.as_string()
                server.sendmail(fromaddr, toaddr, text)
            except:
                return False
            return True

        if mail(mails_to):
            UOlData.objects.filter(status=False, id=u.id).update(write_email=True)


# доп. уведомление о частичном подписани
def send_email_date_write_adv():
    mails_to = ['']

    def mail(mails, u):
        try:
            msg = MIMEMultipart()
            server = smtplib.SMTP('')
            toaddr = mails
            fromaddr = ''
            msg['Subject'] = 'Обходной лист [{0}, увольнение: {1}]'.format(u.employ_id.name,
                                                                           u.date_dism)
            body = "<a href=http://url/hr/ol/details/{0}/>Подписан всеми участниками, " \
                   "ожидание вашей подписи</a>".format(u.id)
            msg.attach(MIMEText(body, 'html', _charset='utf-8'))
            text = msg.as_string()
            server.sendmail(fromaddr, toaddr, text)
        except:
            return False
        return True

    uolda = UOlData.objects.filter(Q(write_email_adv=None) | Q(write_email_adv=False))
    for u in uolda:
        uolsign = UOlDataAdvSign.objects.filter(uoldata=u)
        f = False
        i = 0
        for ud in uolsign:
            i = i + 1
            uu = UOlDataAdvSign.objects.filter(uoldata=u, id=ud.id, status=False)
            if uu:
                if ud.author.id != 427:  # черноштанова
                    f = True
                    continue
            if len(uolsign) == i:
                if not f:
                    if mail(mails_to, u):
                        UOlData.objects.filter(id=u.id).update(write_email_adv=True)


# уведомление о последнем дне подписания
def send_email_date_write_lastday():
    def mail(mails, u):
        try:
            msg = MIMEMultipart()
            server = smtplib.SMTP('')
            toaddr = mails
            fromaddr = ''
            msg['Subject'] = 'Обходной лист [{0}, увольнение: {1}]'.format(u.employ_id.name,
                                                                           u.date_dism)
            body = "<a href=http://url/hr/ol/details/{0}/>" \
                   "Сегодня увольняется сотрудник. " \
                   "Нужна ваша подпись</a>".format(u.id)
            msg.attach(MIMEText(body, 'html', _charset='utf-8'))
            text = msg.as_string()
            server.sendmail(fromaddr, toaddr, text)
        except:
            return False
        return True

    uolda = UOlData.objects.filter(Q(write_email_lastday=None) | Q(write_email_lastday=False))
    for u in uolda:
        if u.date_dism == date.today():
            uolsign = UOlDataAdvSign.objects.filter(uoldata=u, status=False)
            for ud in uolsign:
                mails_to = []
                uead = UsersExtLynxEmploys.objects.get(employ_id=ud.author.employ_id)
                ad = UsersExtAD.objects.get(employ_id_id=uead)
                mails_to.append(ad.email)
                if mail(mails_to, u):
                    UOlData.objects.filter(id=u.id).update(write_email_lastday=True)
