import six
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import user_passes_test
from .models import General


def group_required_admin(view=None, group=None, login_url=None, raise_exception=False):
    def check_perms(user):
        try:
            group = General.objects.get(id=1)
            # общая группа доступа
            general_groups = group.general_groups_admin  # admin
            if isinstance(general_groups, six.string_types):
                groups = (general_groups,)
            else:
                groups = general_groups
        except:
            return False
        if user.groups.filter(name=groups).exists():
            return True
        return False
    return user_passes_test(check_perms)


def check_perms_admin(user):
    try:
        group = General.objects.get(id=1)
        # общая группа доступа
        general_groups = group.general_groups_admin  # admin
        if isinstance(general_groups, six.string_types):
            groups = (general_groups,)
        else:
            groups = general_groups
    except:
        return False
    if user.groups.filter(name=groups).exists():
        return True
    return False


