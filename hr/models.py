from django.db import models

from users_info.models import UsersExtLynxEmploys
from django.contrib.auth.models import Group
from django.db import models


class Podr(models.Model):
    podr = models.CharField('Подразделение', max_length=80, unique=True)

    class Meta:
        verbose_name = '[Обходной лист] подразделения'
        verbose_name_plural = 'Подразделения'

    def __str__(self):
        return str(self.podr)


class UPodr(models.Model):
    podr = models.ForeignKey(Podr, verbose_name='Подразделение',
                             on_delete=models.CASCADE, related_name='podr_set')
    employ_id = models.ForeignKey(UsersExtLynxEmploys, verbose_name='Подпись от',
                                  on_delete=models.CASCADE, related_name='usersextemploysupodr_set')

    class Meta:
        verbose_name = '[Обходной лист] связь пользователей и подразделений'
        verbose_name_plural = 'Связь пользователей и подразделений'
        unique_together = ['podr', 'employ_id']

    def __str__(self):
        return str(self.podr)


class UOlData(models.Model):
    employ_id = models.ForeignKey(UsersExtLynxEmploys, verbose_name='Уволяющийся',
                                  on_delete=models.CASCADE, related_name='usersextemploysuoldata_set')

    date_create = models.DateTimeField('Дата создания', auto_now_add=True)
    date_write = models.DateTimeField('Дата полного подписания', blank=True, null=True)
    date_dism = models.DateField('Дата увольнения', blank=True, null=True)
    date_close = models.DateTimeField('Дата закыртия', blank=True, null=True)
    status = models.BooleanField('Статус листа, закрыт?')
    write_email = models.BooleanField('Отправлено сообщение о полном подписании?', null=True, blank=True)
    write_email_adv = models.BooleanField('Отправлено сообщение о частичном подписании?', null=True, blank=True)
    write_email_lastday = models.BooleanField('Отправлено сообщение о последний день подписания?', null=True,
                                              blank=True)

    class Meta:
        verbose_name = 'основные данные'
        verbose_name_plural = 'Основные данные'

    def __str__(self):
        return str(self.employ_id)


class UOlDataAdvComments(models.Model):
    author = models.ForeignKey(UsersExtLynxEmploys, verbose_name='ИД пользователя Lynx_Employs',
                               on_delete=models.CASCADE, related_name='usersextemploysuoldataadvc_set')

    uoldata = models.ForeignKey(UOlData, verbose_name='ИД обх. лист',
                                on_delete=models.CASCADE, related_name='uoldataadvcomments_set')

    comment = models.TextField(max_length=255, verbose_name='Комментарий')
    date_create = models.DateTimeField('Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = 'комментарии'
        verbose_name_plural = 'Комментарии'

    def __str__(self):
        return str(self.author.name)


class UOlDataAdvSign(models.Model):
    author = models.ForeignKey(UsersExtLynxEmploys, verbose_name='Подписавший',
                               on_delete=models.CASCADE, related_name='usersextemploysuoldataadvs_set')

    uoldata = models.ForeignKey(UOlData, verbose_name='Обх. лист',
                                on_delete=models.CASCADE, related_name='uoldataadvsign_set')

    date_sign = models.DateTimeField('Дата подписи', blank=True, null=True)
    status = models.BooleanField('Подписано?', blank=True)
    new_email_write = models.BooleanField('Email о новом ОЛ', null=True, blank=True)

    class Meta:
        verbose_name = 'подписи'
        verbose_name_plural = 'Подписи'
        ordering = ['date_sign']

    def __str__(self):
        return str(self.author.name)


class General(models.Model):
    general_groups_admin = models.ForeignKey(Group, related_name='hr_general_groups_admin',
                                             verbose_name='Администратор группы',
                                             on_delete=models.CASCADE, null=True)

    general_groups = models.ForeignKey(Group, related_name='hr_general_groups',
                                       verbose_name='Разрешение для группы',
                                       on_delete=models.CASCADE, null=True)

    hr_ol_general_groups_write_email = models.ForeignKey(Group, related_name='hr_ol_general_groups_write_email',
                                                         verbose_name='Кому отправится сообщение о полном подписании',
                                                         on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = 'Группа доступа'
        verbose_name_plural = 'группа доступа'

    def __str__(self):
        return str(self.general_groups)
