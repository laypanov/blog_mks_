import datetime
from _testcapi import raise_exception

import six
from django import template
from django.core.exceptions import PermissionDenied

from hr.models import General

register = template.Library()


@register.filter
def get_perm_create(user):
    try:
        group = General.objects.get(id=1)
        # общая группа доступа
        general_groups = group.general_groups_admin  # admin
        if isinstance(general_groups, six.string_types):
            groups = (general_groups,)
        else:
            groups = general_groups
    except:
        return ""
    if user.groups.filter(name=groups).exists():
        return "Создать"
    return ""


@register.filter
def get_perm_config(user):
    try:
        group = General.objects.get(id=1)
        # общая группа доступа
        general_groups = group.general_groups_admin  # admin
        if isinstance(general_groups, six.string_types):
            groups = (general_groups,)
        else:
            groups = general_groups
    except:
        return ""
    if user.groups.filter(name=groups).exists():
        return "Настройки"
    return ""


# users_info
@register.filter
def get_vocation(queryset):
    from datetime import date, datetime

    today = date.today()
    today = today.strftime("%Y-%m-%d")
    queryset = queryset.strftime("%Y-%m-%d")
    if queryset >= today:
        date = datetime.strptime(queryset, "%Y-%m-%d")
        rd = (datetime.strptime(queryset, "%Y-%m-%d") - datetime.now()).days + 2
        date = date.strftime("%d.%m")
        rd = "%s [%s]" % (date, rd)
        return rd
    else:
        return ""
