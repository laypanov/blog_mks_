from django.conf import settings
from django.conf.urls import url
from django.templatetags.static import static
from django.urls import path

from . import views

from .views import config

app_name = 'hr'

urlpatterns = [
    url(r'^ol/$', views.ol, name="hr_ol"),
    url(r'^ol/details/(?P<slug>.+)/close_ol/$', views.close_ol, name="close_ol"),
    url(r'^ol/details/(?P<slug>.+)/add_comment/$', views.add_comment, name="add_comment"),
    url(r'^ol/details/(?P<slug>.+)/$', views.details, name="hr_details"),
]
