from django.contrib import admin
from .models import Version, Program


class ProgramInstanceAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': (
                'name',
            )
        }),
    )
    list_display = [
        'name',
    ]


admin.site.register(Program, ProgramInstanceAdmin)


class VersionInstanceAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': (
                'name_program',
                'program',
                'actual',
                'visible',
                'build',
                'date',
                'link',
            )
        }),
    )
    list_display = [
        'program',
        'actual',
        'visible',
        'build',
        'date',
        'link',
    ]


admin.site.register(Version, VersionInstanceAdmin)
