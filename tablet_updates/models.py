from django.db import models


class Program(models.Model):
    name = models.CharField('Название приложения', max_length=255)

    class Meta:
        ordering = ('name',)
        verbose_name = 'название ПО'
        verbose_name_plural = 'Название ПО'
        app_label = 'tablet_updates'

    def __str__(self):
        return self.name


class Version(models.Model):
    name_program = models.CharField('Локальное название программы', max_length=100)
    program = models.ForeignKey(Program, on_delete=models.PROTECT,
                                verbose_name='программа',
                                )
    actual = models.BooleanField('Актуальная версия')
    visible = models.BooleanField('Доступна для скачивания')
    build = models.CharField('Версия сборки', max_length=255)
    date = models.DateField('Дата публикации')
    link = models.FileField('Файл', upload_to='tablet_updates')
    pic = models.ImageField('Иконка', upload_to='tablet_updates')

    class Meta:
        ordering = ('date',)
        verbose_name = 'версии файлов'
        verbose_name_plural = 'Версии файлов'
        app_label = 'tablet_updates'

    def __str__(self):
        return self.name_program
