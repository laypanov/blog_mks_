from django.apps import AppConfig


class TabletUpdatesConfig(AppConfig):
    name = 'tablet_updates'
    verbose_name = 'ПО склад(Обнолвятор)'
