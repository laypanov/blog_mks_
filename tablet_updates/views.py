from django.shortcuts import render
from .models import Program, Version
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def get_info(request):
    versions = Version.objects.values('program__name', 'name_program', 'actual', 'visible', 'build', 'date', 'link')\
        .filter(visible='True').order_by('name_program', '-actual')
    return JsonResponse(dict(versions=list(versions)))


