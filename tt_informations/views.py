import codecs
import csv
import itertools
import json
import os
import random
import uuid

import pdfkit
import requests
import urllib3
from django.template.loader import render_to_string

from blog_mks import settings
from .forms import PostFormQuestionnaires
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from .conn_pass import *

from django.core.mail import send_mail

from .models import Questionnaires, PhotoReport, PhotoStore, Author, TTInfo
from .utils import group_required
from django.db.models import Q
from .sql import *


@group_required()
def questionnaires_list(request):
    questionnaires = Questionnaires.objects.all().order_by('-date_created')  # [:100200]
    count = Questionnaires.objects.all().count()
    count_export = Questionnaires.objects.filter(export=True, rec_id__contains='pt').count()
    return render(request, "tt_informations/categories.html", {
        'questionnaires': questionnaires,
        'count': count,
        'count_export': count_export})


@group_required()
def maps(request):
    questionnaires = Questionnaires.objects.filter(export=False, rec_id__contains='pt')
    ttinfo = TTInfo.objects.all()
    return render(request, "tt_informations/maps.html", {'questionnaires': questionnaires, 'ttinfo': ttinfo, })


@group_required()
def change_status(request):
    if request.is_ajax and request.method == "POST":
        rec_id = request.POST.get('rec_id')
        id = rec_id.split("[")
        id = id[1].split("]")
        cs = request.POST.get('cs')
        author_uuid = request.POST.get('author_uuid')
        a = Author.objects.get(uuid=author_uuid)
        if cs == "1":
            Questionnaires.objects.filter(author=a, id=id[0]).update(export=True)
        else:
            Questionnaires.objects.filter(author=a, id=id[0]).update(export=False)
    return HttpResponse(json.dumps("done"), content_type="application/json;charset=utf-8")


@group_required()
def save_textarea(request):
    if request.is_ajax and request.method == "POST":
        rec_id = request.POST.get('rec_id')
        id = rec_id.split("[")
        id = id[1].split("]")
        textarea = request.POST.get('textarea')
        author_uuid = request.POST.get('author_uuid')
        a = Author.objects.get(uuid=author_uuid)
        Questionnaires.objects.filter(author=a, id=id[0]).update(comment=textarea)
    return HttpResponse(json.dumps("done"), content_type="application/json;charset=utf-8")


@group_required()
def question_detail(request, slug, pdf=None):
    latitude = None
    longitude = None
    path_image = ""
    if request.method == "POST":
        pdf = True
        latitude = request.POST.get('latitude')
        longitude = request.POST.get('longitude')
        pdf_download = request.POST.get('pdf_download')
        static_url = "https://api.mapbox.com/styles/v1/mapbox/light-v10/static/" \
                     "pin-s-t+000({0},{1})/{0},{1},18/1280x400?access_token=" \
                     "" \
            .format(longitude, latitude)

        def download(url, path, chunk=2048):
            req = requests.get(url, stream=True)
            if req.status_code == 200:
                with open(path, 'wb') as f:
                    for chunk in req.iter_content(chunk):
                        f.write(chunk)
                    f.close()
                return path
            raise Exception('Given url is return status code:{}'.format(req.status_code))

        path_image = "/tmp/pdf/%s.jpg" % uuid.uuid1(random.randint(0, 281474976710655))
        download(static_url, settings.MEDIA_ROOT + path_image)

    questionnaires = get_object_or_404(Questionnaires, id=slug)
    photo_report = PhotoReport.objects.filter(rec_id=questionnaires.id)
    photo_consumer = PhotoStore.objects.filter(rec_id=questionnaires.id)
    author_uuid = questionnaires.author.uuid

    if questionnaires.latitude and questionnaires.longitude:
        s = sql_get_nn(questionnaires.latitude, questionnaires.longitude)
        cursor.execute(s)
        nn = cursor.fetchone()
        if nn:
            nn = nn[0]
        else:
            nn = 'Нет'

    ins = {
        'questionnaires': questionnaires,
        'nn': nn,
        'photo_report': photo_report,
        'photo_consumer': photo_consumer,
        'author_uuid': author_uuid,
        'path_image': path_image
    }
    # Если хотим создать PDF
    if pdf:
        content = render_to_string("tt_informations/create_pdf.html", ins)
        content = content.replace("src=\"", "src=\"%s" % settings.BASE_URL)
        content = content.replace("href=\"", "href=\"%s" % settings.BASE_URL)
        file_path = 'media/tmp/tt_informations/PDF_%s.pdf' % random.randrange(100000)
        pdfkit.from_string(content, file_path)

        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/pdf")

                if pdf_download:
                    response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_path)
                else:
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                return response
    return render(request, "tt_informations/details.html", ins)


@group_required()
def new(request):
    if request.method == "POST":
        form = PostFormQuestionnaires(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            a = Author.objects.get(uuid=uuid_author)
            post.author_id = a.id
            try:
                q = Questionnaires.objects.filter(author=Author.objects.get(uuid=uuid_author)).order_by('-rec_id')[0]
                rec_id = int(q.rec_id[3:]) + 1
            except:
                rec_id = 1

            post.rec_id = "st_" + str(rec_id)
            post.save()
            return redirect('ql')
    else:
        form = PostFormQuestionnaires()
    return render(request, "tt_informations/new.html", {'form': form})


@group_required()
def edit(request, slug):
    post = get_object_or_404(Questionnaires, id=slug)
    if request.method == "POST":
        form = PostFormQuestionnaires(request.POST, instance=post)
        if form.is_valid():
            q = Questionnaires.objects.get(id=slug)
            post = form.save(commit=False)
            post.author_id = q.author_id
            post.rec_id = q.rec_id
            post.save()
            return redirect('question_detail', slug=post.id)
    else:
        form = PostFormQuestionnaires(instance=post)
    return render(request, "tt_informations/edit.html", {'form': form})


@group_required()
def export_xls(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tt_informations.csv"'
    response.write(u'\ufeff'.encode('utf8'))

    writer = csv.writer(response, dialect='excel')

    writer.writerow(
        ['rec_id', 'name', 'address', 'full_address', 'town', 'region', 'latitude', 'longitude', 'kateg_f', 'name_net',
         'ul', 'inn', 'torgter', 'addressd', 'tip', 's_i', 's_n', 'formtorg', 'tel', 'contact', 'post', 'kanal',
         'sostav', 'post_pa', 'date_created', 'date_added', 'author_id', 'export', 'comment', 'send_email', 'email',
         'kpp', 'ogrn', 'okpo', 'okohn', 'mfo', 'rs', 'ks', 'bik', 'bank', 'address_bank_u', 'address_bank_f', 'p_nds'])

    questionnaires = Questionnaires.objects.all().values_list(
        'rec_id', 'name', 'address', 'full_address', 'town', 'region', 'latitude', 'longitude', 'kateg_f', 'name_net',
        'ul', 'inn', 'torgter', 'addressd', 'tip', 's_i', 's_n', 'formtorg', 'tel', 'contact', 'post', 'kanal',
        'sostav', 'post_pa', 'date_created', 'date_added', 'author_id', 'export', 'comment', 'send_email', 'email',
        'kpp', 'ogrn', 'okpo', 'okohn', 'mfo', 'rs', 'ks', 'bik', 'bank', 'address_bank_u', 'address_bank_f', 'p_nds')

    for q in questionnaires:
        writer.writerow(q)

    return response


def send_email_analitik():
    qes = Questionnaires.objects.filter(Q(send_email=None) | Q(send_email=False))
    if qes.count() > 0:
        send_mail('Аналитика ТТ - новые анкеты (%s)' % qes.count(), '', '',
                  ['', '', ''],
                  fail_silently=False)
        qes.update(send_email=True)
