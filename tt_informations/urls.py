from django.conf import settings
from django.conf.urls import url
from django.templatetags.static import static
from django.urls import path

from . import views

#from .views import view #, detail_view

app_name = 'tt_informations'

if settings.ADMIN_ENABLED:
    urlpatterns = [
        url(r'^$', views.questionnaires_list, name='questionnaires_list'),
        url(r'^question/change_status/$', views.change_status, name="change_status"),
        url(r'^question/save_textarea/$', views.save_textarea, name="save_textarea"),
        url(r'^question/(?P<slug>.+)/edit/$', views.edit, name="edit"),
        url(r'^question/new/$', views.new, name="new"),
        url(r'^question/(?P<slug>.+)/$', views.question_detail, name="question_detail"),
    ]
else:
    urlpatterns = [
    ]
