import os
import urllib
import uuid as uuid

from django.core.files import File
from django.db import models
from django.contrib.auth.models import Group


class TTInfo(models.Model):
    rec_id = models.IntegerField('rec_id')
    name = models.CharField('Название', max_length=80)
    address = models.CharField('Адрес', max_length=190, blank=True)
    latitude = models.CharField('Широта', max_length=15)
    longitude = models.CharField('Долгота', max_length=15)
    town = models.CharField('Город', max_length=255, blank=True)
    region = models.CharField('Область \\ регион', max_length=255, blank=True)
    kateg_f = models.CharField('Категория клиента', max_length=255, blank=True)
    name_net = models.CharField('Гр. компаний', max_length=255, blank=True)
    torgter = models.CharField('Торг. территория', max_length=255, blank=True)
    tip = models.CharField('Тип. ТТ', max_length=255, blank=True)
    s_i = models.CharField('Площадь всего', max_length=255, blank=True)
    s_n = models.CharField('Площадь нашего ассортимента', max_length=255, blank=True)
    formtorg = models.CharField('Формат торговли', max_length=255, blank=True)
    tel = models.CharField('Контактный номер', max_length=255, blank=True)
    contact = models.CharField('Контактное лицо', max_length=255, blank=True)
    post = models.CharField('Должность', max_length=255, blank=True)
    kanal = models.CharField('Канал сбыта', max_length=255, blank=True)
    sostav = models.CharField('Состав канала', max_length=255, blank=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'действующие ТТ'
        verbose_name_plural = 'действующие ТТ'
        app_label = 'tt_informations'

    def __str__(self):
        return str(self.name)


class Author(models.Model):
    last_name = models.CharField('Фамилия', max_length=80)
    first_name = models.CharField('Имя', max_length=80)
    middle_name = models.CharField('Отчество', max_length=80)
    uuid = models.CharField(max_length=36, blank=True, unique=True, default=uuid.uuid4)

    class Meta:
        ordering = ('first_name',)
        verbose_name = 'Пользователи приложений'
        verbose_name_plural = 'пользователи приложений'
        app_label = 'tt_informations'

    def __str__(self):
        return str(self.last_name + " " + self.first_name[:1].upper() + "." + self.middle_name[:1].upper() + ".")


class Questionnaires(models.Model):
    rec_id = models.CharField('rec_id', max_length=80)
    name = models.CharField('Название ТТ', max_length=80)
    address = models.CharField('Адрес', max_length=190, blank=True)
    full_address = models.CharField('Полный адрес', max_length=390, blank=True)
    town = models.CharField('Город', max_length=190, blank=True)
    region = models.CharField('Область \\ регион', max_length=190, blank=True)
    latitude = models.CharField('Широта', max_length=15)
    longitude = models.CharField('Долгота', max_length=15)
    kateg_f = models.CharField('Категория клиента', blank=True, max_length=255)
    name_net = models.CharField('Гр. компаний', blank=True, max_length=255)
    ul = models.CharField('Юр. лицо', max_length=255, blank=True)
    inn = models.CharField('ИНН', max_length=255, blank=True)
    torgter = models.CharField('Торг. территория', max_length=255, blank=True)
    addressd = models.CharField('Адрес доставки', max_length=255, blank=True)
    tip = models.CharField('Тип. ТТ', max_length=255, blank=True)
    s_i = models.CharField('Площадь всего', max_length=255, blank=True)
    s_n = models.CharField('Площадь нашего ассортимента', max_length=255, blank=True)
    formtorg = models.CharField('Формат торговли', max_length=255, blank=True)
    tel = models.CharField('Контактный номер', max_length=255, blank=True)
    contact = models.CharField('Контактное лицо', max_length=255, blank=True)
    post = models.CharField('Должность', max_length=255, blank=True)
    kanal = models.CharField('Канал сбыта', max_length=255, blank=True)
    sostav = models.CharField('Состав канала', max_length=255, blank=True)
    post_pa = models.CharField('Поставщик по нашему ассортименту', max_length=255, blank=True)
    date_created = models.DateTimeField('Время создания', blank=True, null=True)
    date_added = models.DateTimeField('Время добавления на сервер', auto_now_add=True)
    export = models.BooleanField("Выгружен в Рысь")
    comment = models.TextField('Комментарий', max_length=255, blank=True)
    send_email = models.BooleanField('Уведомление на почту', blank=True)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name='questionnaires_set')

    # реквизиты для банка
    email = models.EmailField('Электронная почта', blank=True)
    kpp = models.IntegerField('КПП', blank=True)
    ogrn = models.IntegerField('ОГРН', blank=True)
    okpo = models.IntegerField('ОКПО', blank=True)
    okohn = models.IntegerField('ОКОХН', blank=True)
    mfo = models.IntegerField('МФО', blank=True)
    rs = models.IntegerField('р\счет', blank=True)
    ks = models.IntegerField('к\счет', blank=True)
    bik = models.IntegerField('БИК', blank=True)
    bank = models.CharField('БАНК', max_length=255, blank=True)
    address_bank_u = models.CharField('Адрес юридический/индекс', max_length=255, blank=True)
    address_bank_f = models.CharField('Адрес фактический/индекс', max_length=255, blank=True)
    p_nds = models.CharField('Плательщик НДС', max_length=255, blank=True)

    # блок для работы аналитиков
    a_add_tt = models.BooleanField('Добавление ТТ', blank=True)
    a_add_gk = models.BooleanField('Добавление ГК', blank=True)
    a_add_ul = models.BooleanField('Добавление ЮЛ(ЧП)', blank=True)

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Анткеты'
        verbose_name_plural = 'анкеты'
        app_label = 'tt_informations'

    def __str__(self):
        return str(self.rec_id)


# Фотоотчет (подробный)')
class PhotoReport(models.Model):
    rec_id = models.ForeignKey(Questionnaires, on_delete=models.CASCADE, related_name='PhotoReport')
    image = models.ImageField()

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Фотоотчет подробный'
        verbose_name_plural = 'фотоотчет подробный'
        app_label = 'tt_informations'

    def __str__(self):
        return str(self.rec_id)


# Фотоотчет (уголок потребителя)
class PhotoConsumer(models.Model):
    rec_id = models.ForeignKey(Questionnaires, on_delete=models.CASCADE, related_name='PhotoConsumer')
    image = models.ImageField(upload_to='images/')

    class Meta:
        ordering = ('-id',)
        verbose_name = 'уголок потербителя (фото)'
        verbose_name_plural = 'уголок потербителя (фото)'
        app_label = 'tt_informations'

    def __str__(self):
        return str(self.rec_id)


# Фотоотчет (фасад магазина)
class PhotoStore(models.Model):
    rec_id = models.ForeignKey(Questionnaires, on_delete=models.CASCADE, related_name='PhotoStore')
    image = models.ImageField(upload_to='images/')

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Фотоотчет фасад магазина'
        verbose_name_plural = 'фотоотчет фасад магазина'
        app_label = 'tt_informations'

    def __str__(self):
        return str(self.rec_id)

