import six
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import user_passes_test


def group_required(view=None, group=None, login_url=None, raise_exception=False):
    def check_perms(user):
        if user.groups.filter(name="Служебный_Аналитика_ТТ").exists():
            return True
        # Если в интересующей нас группе такое имя не фигурирует -
        # кидаемся в пользователя 403-й ошибкой.
        if raise_exception:
            raise PermissionDenied
        # As the last resort, show the login form
        return False
    return user_passes_test(check_perms)

