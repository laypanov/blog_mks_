from django.contrib import admin
from tt_informations.models import TTInfo, Questionnaires, PhotoReport, PhotoStore, Author


class TTInfo(admin.ModelAdmin):
    admin.site.register(TTInfo)


class PhotoReport(admin.ModelAdmin):
    admin.site.register(PhotoReport)


class PhotoStore(admin.ModelAdmin):
    admin.site.register(PhotoStore)


class QuestionnairesInstanceAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('author', 'rec_id', 'comment', 'export', 'send_email')
        }),
        ('Информация о ТТ', {
            'fields': (
                'name',
                'address',
                'town',
                'region',
                'full_address',
                'latitude',
                'longitude',
                'kateg_f',
                'name_net',
                'ul',
                'inn',
                'torgter',
                'addressd',
                'tip',
                's_i',
                's_n',
                'formtorg',
                'tel',
                'contact',
                'post',
                'kanal',
                'sostav',
                'post_pa',
                'date_created',
                'date_added',
            )
        }),
        ('Банковские реквизиты', {
            'fields': (
                'kpp',
                'ogrn',
                'okpo',
                'okohn',
                'mfo',
                'rs',
                'ks',
                'bik',
                'bank',
                'address_bank_u',
                'address_bank_f',
                'email',
                'p_nds',
            )
        }),
    )
    list_display = [
        'name',
        'send_email',
        'author',
        'rec_id',
        'tel',
        'full_address',
        'date_created',
        'date_added',
    ]
    readonly_fields = ['date_added', 'date_created']


admin.site.register(Questionnaires, QuestionnairesInstanceAdmin)


class Author(admin.ModelAdmin):
    admin.site.register(Author)

