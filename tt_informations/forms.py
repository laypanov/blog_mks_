from django import forms
from django.forms import TextInput, ChoiceField, TypedChoiceField, Select, IntegerField, NumberInput, BooleanField, \
    RadioSelect

from .models import PhotoReport, PhotoStore, PhotoConsumer, Questionnaires


class PhotoReportImageForm(forms.ModelForm):
    # rec_id = forms.IntegerField()
    image = forms.ImageField(label='image')

    class Meta:
        model = PhotoReport
        fields = [
            # 'rec_id',
            'image',
        ]


class PhotoStoreImageForm(forms.ModelForm):
    rec_id = forms.IntegerField()
    image = forms.ImageField(label='image')

    class Meta:
        model = PhotoStore
        fields = [
            'rec_id',
            'image',
        ]


class PhotoConsumerImageForm(forms.ModelForm):
    rec_id = forms.IntegerField()
    image = forms.ImageField(label='image')

    class Meta:
        model = PhotoConsumer
        fields = [
            'rec_id',
            'image',
        ]


class PostFormQuestionnaires(forms.ModelForm):
    class Meta:
        model = Questionnaires
        fields = (
            'name',
            'address',
            'town',
            'region',
            'full_address',
            'latitude',
            'longitude',
            'kateg_f',
            'name_net',
            'ul',
            'inn',
            'torgter',
            'addressd',
            'tip',
            's_i',
            's_n',
            'formtorg',
            'tel',
            'contact',
            'post',
            'kanal',
            'sostav',
            'post_pa',
            'comment',

            'kpp',
            'ogrn',
            'okpo',
            'okohn',
            'mfo',
            'rs',
            'ks',
            'bik',
            'bank',
            'address_bank_u',
            'address_bank_f',
            'email',
            'p_nds',

            'a_add_tt',
            'a_add_gk',
            'a_add_ul',
        )
        kateg_f_choices = (
            ('', 'Выберите...'),
            ('A', 'A'),
            ('B', 'B'),
            ('C', 'C'),
        )
        torgter_choices = (
            ('', 'Выберите...'),
        )

        for tt in range(100):
            torgter_choices = torgter_choices + (('Дон_' + str(tt), 'Дон_' + str(tt)),)

        tip_choices = (
            ('', 'Выберите...'),
            ('ТТ', 'ТТ'),
            ('Склад', 'Склад'),
            ('Офис', 'Офис'),
            ('Представительство', 'Представительство'),
            ('Расп. центр', 'Расп. центр'),
        )
        formtorg_choices = (
            ('', 'Выберите...'),
            ('Самообслуживание', 'Самообслуживание'),
            ('Традиционная торговля', 'Традиционная торговля'),
        )
        post_choices = (
            ('', 'Выберите...'),
            ('Менеджер по продажам', 'Менеджер по продажам'),
            ('Директор', 'Директор'),
            ('Продавец', 'Продавец'),
            ('Владелец бизнеса', 'Владелец бизнеса'),
        )
        kanal_choices = (
            ('', 'Выберите...'),
            ('Бизнес для бизнеса', 'Бизнес для бизнеса'),
            ('Магазины', 'Магазины'),
            ('ОПТ', 'ОПТ'),
            ('Рынки', 'Рынки'),
            ('Сети', 'Сети'),
        )
        sostav_choices = (
            ('', '-'),
            ('', 'Выберите...'),
            ('Автопредприятие', 'Автопредприятие'),
            ('Автосервис', 'Автосервис'),
            ('ЖКХ', 'ЖКХ'),
            ('Заводы фабрики', 'Заводы фабрики'),
            ('Образовательные учереждения', 'Образовательные учереждения'),
            ('Ремонтные бригады', 'Ремонтные бригады'),
            ('Силовые структуры', 'Силовые структуры'),
            ('Строительные компании', 'Строительные компании'),
            ('Транспорт', 'Транспорт'),

            ('Авто', 'Авто'),
            ('Инструмент', 'Инструмент'),
            ('Интернет магазин', 'Интернет магазин'),
            ('Лаки и краски', 'Лаки и краски'),
            ('Маркет', 'Маркет'),
            ('Метизы', 'Метизы'),
            ('Прочие', 'Прочие'),
            ('Садовая', 'Садовая'),
            ('Сантехника', 'Сантехника'),
            ('Строительные материалы', 'Строительные материалы'),
            ('Хозяйственные материалы', 'Хозяйственные материалы'),
            ('Электроинструмент', 'Электроинструмент'),
            ('Электрофурнитура', 'Электрофурнитура'),

            ('Оптовик', 'Оптовик'),
            ('Посредник', 'Посредник'),
            ('Прочие', 'Прочие'),

            ('Розница рынки', 'Розница рынки'),

            ('Не специализированные_региональные', 'Не специализированные_региональные'),
            ('Не специализированные_федеральные', 'Не специализированные_федеральные'),
            ('Не специализированные(DIY)_региональные', 'Не специализированные(DIY)_региональные'),
            ('Не специализированные(DIY)_федеральные', 'Не специализированные(DIY)_федеральные'),
        )
        p_nds = (
            ('', 'Выберите...'),
            ('Да', 'Да'),
            ('Нет', 'Нет'),
        )

        BOOL_CHOICES = (('', 'Выберите...'), (True, 'Да'), (False, 'Нет'))

        widgets = {
            "name": TextInput(attrs={'class': 'vTextField'}),
            "address": TextInput(attrs={'class': 'vTextField'}),
            "town": TextInput(attrs={'class': 'vTextField'}),
            "region": TextInput(attrs={'class': 'vTextField'}),
            "full_address": TextInput(attrs={'class': 'vTextField'}),
            "latitude": TextInput(attrs={'class': 'vTextField'}),
            "longitude": TextInput(attrs={'class': 'vTextField'}),
            "kateg_f": Select(choices=kateg_f_choices, attrs={'class': 'form-control vSelectField'}),
            "name_net": TextInput(attrs={'class': 'vTextField'}),
            "ul": TextInput(attrs={'class': 'vTextField'}),
            "inn": NumberInput(attrs={'class': 'vTextField'}),
            "torgter": Select(choices=torgter_choices, attrs={'class': 'form-control vSelectField'}),
            "addressd": TextInput(attrs={'class': 'vTextField'}),
            "tip": Select(choices=tip_choices, attrs={'class': 'form-control vSelectField'}),
            "s_i": NumberInput(attrs={'class': 'vTextField'}),
            "s_n": NumberInput(attrs={'class': 'vTextField'}),
            "formtorg": Select(choices=formtorg_choices, attrs={'class': 'form-control vSelectField'}),
            "tel": TextInput(attrs={'class': 'form-control bfh-phone vTextField',
                                    'data-mask': '+7 (000) 000 00 00',
                                    'placeholder': '+7 (___) ___ __ __'}),
            "contact": TextInput(attrs={'class': 'vTextField'}),
            "post": Select(choices=post_choices, attrs={'class': 'form-control vSelectField'}),
            "kanal": Select(choices=kanal_choices, attrs={'class': 'form-control vSelectField'}),
            "sostav": Select(choices=sostav_choices, attrs={'class': 'form-control vSelectField'}),
            "post_pa": TextInput(attrs={'class': 'vTextField'}),
            "comment": TextInput(attrs={'class': 'vTextField'}),

            "kpp": NumberInput(attrs={'class': 'vTextField'}),
            "ogrn": NumberInput(attrs={'class': 'vTextField'}),
            "okpo": NumberInput(attrs={'class': 'vTextField'}),
            "okohn": NumberInput(attrs={'class': 'vTextField'}),
            "mfo": NumberInput(attrs={'class': 'vTextField'}),
            "rs": NumberInput(attrs={'class': 'vTextField'}),
            "ks": NumberInput(attrs={'class': 'vTextField'}),
            "bik": NumberInput(attrs={'class': 'vTextField'}),
            "bank": TextInput(attrs={'class': 'vTextField'}),
            "address_bank_u": TextInput(attrs={'class': 'vTextField'}),
            "address_bank_f": TextInput(attrs={'class': 'vTextField'}),
            "email": TextInput(attrs={'class': 'vTextField'}),
            "p_nds": forms.Select(choices=p_nds, attrs={'class': 'form-control vSelectField'}),

            "a_add_tt": forms.Select(choices=BOOL_CHOICES, attrs={'class': 'form-control vSelectField'}),
            "a_add_gk": forms.Select(choices=BOOL_CHOICES, attrs={'class': 'form-control vSelectField'}),
            "a_add_ul": forms.Select(choices=BOOL_CHOICES, attrs={'class': 'form-control vSelectField'}),

        }
        labels = {
            'name': 'Название ТТ:',
        }
