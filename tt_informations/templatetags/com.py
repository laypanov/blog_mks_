import re

from django import template

register = template.Library()


@register.filter
def rem_nr(args):
    if args:
        return args.splitlines()[0]
    else:
        return args


@register.filter
def clearz(args):
    if args:
        args = args.split(",")
        return args[0]
    else:
        return args
