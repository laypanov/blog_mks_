from django.apps import AppConfig


class TtInformationsConfig(AppConfig):
    name = "tt_informations"
    verbose_name = 'Аналитика ТТ (внешняя БД)'
