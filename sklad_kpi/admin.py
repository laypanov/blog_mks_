from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import RL, General


class RLInstanceAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': (
                'fio',
                'section',
                'ship_lines',
                'asbl_lines',
                'user_bonus',
                'salary',
            )
        }),
    )
    list_display = [
        'fio',
        'section',
        'ship_lines',
        'asbl_lines',
        'user_bonus',
        'salary',
    ]


admin.site.register(RL, RLInstanceAdmin)

admin.site.register(
    General,
    SingletonModelAdmin,
    fields=[
        'general_groups_admin',
        'general_groups',
    ]
)

