from django.apps import AppConfig


class SkladKpiConfig(AppConfig):
    name = 'sklad_kpi'
    verbose_name = 'Данные с XLS (Склад)'

