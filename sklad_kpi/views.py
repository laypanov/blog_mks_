import openpyxl
from django.http import HttpResponse
from django.shortcuts import render
from .utils import group_required
from sklad_kpi.models import RL


@group_required()
def view_users_kpi(request):
    try:
        if request.method == 'POST' and request.FILES['myfile']:
            myfile = request.FILES['myfile']
            create_users_kpi(myfile)

        users = RL.objects.all().order_by('fio')
        context = {
            'users': users,
        }
        return render(request, 'sklad_kpi/main.html', context)
    except:
        return render(request, 'sklad_kpi/main.html')


def view_users_kpi_simple(request):
    users = RL.objects.all().order_by('fio')
    context = {
        'users': users,
    }
    return render(request, 'sklad_kpi/simple.html', context)


def xstr(s):
    if s is None:
        return ''
    elif s == '':
        s = 0
    elif isinstance(s, float):
        s = int(s)
    return str(s)


def create_users_kpi(file):
    wb = openpyxl.load_workbook(file)
    ws = wb.active
    rows = ws.max_row
    cols = ws.max_column

    RL.objects.all().delete()

    for i in range(1, rows + 1):
        string = ''
        for j in range(1, cols + 1):
            cell = ws.cell(row=i, column=j)
            string = string + str(cell.value) + ' '
            name = cell.value

        if i > 1 and name is not None:
            fio = ws.cell(row=i, column=1).value
            section = xstr(ws.cell(row=i, column=2).value)
            ship_lines = xstr(ws.cell(row=i, column=3).value)
            asbl_lines = xstr(ws.cell(row=i, column=4).value)
            user_bonus = xstr(ws.cell(row=i, column=5).value)
            salary = xstr(ws.cell(row=i, column=6).value)
            print(fio, section, ship_lines, asbl_lines, user_bonus, salary)
            if fio:
                query = RL(
                    fio=fio,
                    section=section,
                    ship_lines=ship_lines,
                    asbl_lines=asbl_lines,
                    user_bonus=user_bonus,
                    salary=salary,
                )
                query.save()
