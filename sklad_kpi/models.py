from django.db import models
from django.contrib.auth.models import Group


class RL(models.Model):
    fio = models.CharField('ФИО', max_length=255)
    section = models.CharField('Секция', max_length=5)
    ship_lines = models.CharField('Отгружено строк', max_length=10)
    asbl_lines = models.CharField('Собрано строк', max_length=10)
    user_bonus = models.CharField('Личный бонус', max_length=10)
    salary = models.CharField('Личный бонус', max_length=10)

    class Meta:
        ordering = ('fio',)
        verbose_name = 'данные по сборке счетов'
        verbose_name_plural = 'Данные по сборке счетов'
        app_label = 'sklad_kpi'

    def __str__(self):
        return self.fio


class General(models.Model):
    general_groups_admin = models.ForeignKey(Group, related_name='sklad_kpi_general_groups_admin',
                                             verbose_name='Администратор группы',
                                             on_delete=models.CASCADE, null=True)

    general_groups = models.ForeignKey(Group, related_name='sklad_kpi_general_groups',
                                       verbose_name='Разрешение для группы',
                                       on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = 'Группа доступа'
        verbose_name_plural = 'группа доступа'

    def __str__(self):
        return str(self.general_groups)
