import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import re

from tt_informations.models import TTInfo


def check_locations_tt(request):
    tt = TTInfo.objects.all()
    for t in tt:
        lat = t.latitude
        lon = t.longitude
        name = t.name
        name_net = t.name_net

        if lat and lat is not None:
            pattern_lat = re.compile("^(\+|-)?(?:90(?:(?:\.0{1,20})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,20})?))$")
            pattern_lon = re.compile(
                "^(\+|-)?(?:180(?:(?:\.0{1,20})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,20})?))$")

            if pattern_lat.match(lat) is None or pattern_lon.match(lat) is None:
                print(pattern_lat.match(lat), name, lat)
                print(pattern_lon.match(lon), name, lon)
                mail_subject = 'Некорректно заданы координаты'
                mail_receive = ''
                mail_body = '<p>ТТ: {0}</p>' \
                            '<p>Группа компаний: {1}</p>' \
                            '<p>Широта: {2}</p>' \
                            '<p>Долгота: {3}</p>' \
                            'Необходимо исправить'.format(name, name_net, lat, lon)
                send_emails(mail_receive, mail_subject, mail_body)


def send_emails(mail_receive, mail_subject, mail_body):
    try:
        creator_email = ''
        msg = MIMEMultipart()
        server = smtplib.SMTP('')
        toaddr = '%s' % mail_receive  # mails
        cc = '%s' % creator_email
        reply = '%s' % creator_email
        fromaddr = ''

        rcpt = [toaddr] + [cc] + [reply] + [fromaddr]
        msg['Subject'] = mail_subject
        msg['to'] = toaddr
        msg['cc'] = cc
        msg['reply-to'] = reply

        msg.attach(MIMEText(mail_body, 'html', _charset='utf-8'))

        text = msg.as_string()
        server.sendmail(fromaddr, rcpt, text)
    except:
        pass
