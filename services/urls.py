from django.urls import path

from .views import check_locations_tt

app_name = 'services'

urlpatterns = [
    path('get/check_locations_tt', check_locations_tt,
         name="check_locations_tt"),
]
