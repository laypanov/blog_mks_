from django.contrib import admin
from .models import Category, Post, General, AttachmentsModel
from mptt.admin import DraggableMPTTAdmin
from ckeditor.fields import RichTextField
from solo.admin import SingletonModelAdmin


class AttachmentInlines(admin.StackedInline):
    model = AttachmentsModel
    exclude = ()
    extra = 1
    fields = ['file', 'date_edit']
    readonly_fields = ['date_edit']
    list_display = [
        'file',
        'date_edit',
    ]


class Detals(admin.ModelAdmin):
    content = RichTextField()
    fields = [
        'title',
        'category',
        'content',
        'post_groups',
        'tags',
        # 'attachments_model',
        'published',
        'date_edit',
        'date_create',
        'slug',
    ]
    list_display = [
        'title',
        'category',
        'published',
        'date_create',
        'date_edit',
    ]
    readonly_fields = ['date_edit', 'date_create']
    prepopulated_fields = {'slug': ('title',)}
    filter_horizontal = ('post_groups',)
    inlines = (AttachmentInlines,)


admin.site.register(
    AttachmentsModel,
    fields=[
        'file',
        'attachments_model',
        'date_edit',
    ],
    readonly_fields=['date_edit']
)

admin.site.register(
    Post,
    Detals
)

admin.site.register(
    Category,
    DraggableMPTTAdmin,

    prepopulated_fields={'slug': ('name',)}
)

admin.site.register(
    General,
    SingletonModelAdmin,
    fields=[
        'general_groups_admin',
        'general_groups',
    ]
)
