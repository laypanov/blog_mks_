import os
import random

import pdfkit
from django.core.exceptions import PermissionDenied
from private_storage.models import PrivateFile
from private_storage.storage.files import PrivateFileSystemStorage

from .models import Category, AttachmentsModel
from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
from .models import Post
from taggit.models import Tag
from django.template.loader import render_to_string
from django.conf import settings
from .utils import group_required
from blog_mks.utils import group_required_post
from blog_mks.views import h404
from private_storage.views import PrivateStorageView


@group_required()
def create_pdf(request, slug):
    post = get_object_or_404(Post, slug=slug)
    context = {
        'post': post,
    }
    return render(request, 'regulations/detail.html', context)


@group_required()
def detail_view(request, slug):
    post = get_object_or_404(Post, slug=slug)
    context = {
        'post': post,
    }
    return render(request, 'regulations/detail.html', context)


@group_required()
def tagged(request, slug):
    tag = get_object_or_404(Tag, slug=slug)
    common_tags = Post.tags.most_common()[:4]
    # Разрешено просматривать только те посты в которые включен пользователь по группе
    creators_list = group_required_post(request)
    posts = Post.objects.filter(tags=tag, post_groups__name__in=creators_list, published=True).distinct()

    context = {
        'tag': tag,
        'common_tags': common_tags,
        'posts': posts,
        'menu_items': Category.objects.all(),
        'menu_items_filter': Category.objects.filter(
            post__published=True,
            post__post_groups__name__in=creators_list,
        ).distinct(),
        'menu_items_post_count': posts.count(),
    }
    return render(request, 'regulations/tagged.html', context)


@group_required()
def show_category(request, hierarchy=None, pdf=None):
    category_slug = hierarchy.split('/')
    parent = None
    root = Category.objects.all()
    creators_list = group_required_post(request)
    for slug in category_slug[:-1]:
        parent = root.get(parent=parent, slug=slug)
    try:
        instance = Category.objects.get(parent=parent, slug=category_slug[-1])
        instance = instance.post_set.filter(post_groups__name__in=creators_list, published=True).distinct()
    except:
        try:
            instance = Post.objects.filter(
                slug=category_slug[-1],
                post_groups__name__in=creators_list,
                published=True).distinct()
            posts = Post.objects.filter(post_groups__name__in=creators_list, published=True).distinct()
            menu_items_post_count = posts.count()

            if instance.exists():
                instance = get_object_or_404(Post, slug=category_slug[-1])
                # Если хотим создать PDF
                if pdf:
                    content = render_to_string("regulations/create_pdf.html", {
                        'instance': instance
                    })

                    content = content.replace("src=\"", "src=\"%s" % settings.BASE_URL)
                    content = content.replace("href=\"", "href=\"%s" % settings.BASE_URL)
                    file_path = 'media/tmp/regulations/PDF_%s.pdf' % random.randrange(100000)
                    pdfkit.from_string(content, file_path)

                    if os.path.exists(file_path):
                        with open(file_path, 'rb') as fh:
                            response = HttpResponse(fh.read(), content_type="application/pdf")
                            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                            return response

                menu_items = Category.objects.all()
                menu_items_filter = Category.objects.filter(
                    post__published=True,
                    post__post_groups__name__in=creators_list,
                ).distinct()
                return render(request, "regulations/post_detail.html", {
                    'instance': instance,
                    'menu_items': menu_items,
                    'menu_items_filter': menu_items_filter,
                    'menu_items_post_count': menu_items_post_count,
                })
            else:
                return h404(request)
        except:
            return h404(request)

    else:
        post = Post.objects.filter(
            post_groups__name__in=creators_list,
            category__slug=category_slug[-1],
            published=True).distinct()
        menu_items = Category.objects.all()
        menu_items_filter = Category.objects.filter(
            post__published=True,
            post__post_groups__name__in=creators_list,
        ).distinct()
        menu_items_post_count = post.count()

        return render(request, 'regulations/categories.html', {
            'posts': post,
            'menu_items': menu_items,
            'menu_items_filter': menu_items_filter,
            'menu_items_post_count': menu_items_post_count,
        })


@group_required()
def post_list(request):
    # Разрешено просматривать только те посты в которые включен пользователь по группе
    creators_list = group_required_post(request)

    posts = Post.objects.filter(post_groups__name__in=creators_list, published=True).distinct()
    menu_items = Category.objects.all()
    menu_items_filter = Category.objects.filter(
        post__published=True,
        post__post_groups__name__in=creators_list,
    ).distinct()
    menu_items_post_count = posts.count()
    return render(request, "regulations/categories.html", {'posts': posts,
                                                           'menu_items': menu_items,
                                                           'menu_items_filter': menu_items_filter,
                                                           'menu_items_post_count': menu_items_post_count,
                                                           })


class StorageView(PrivateStorageView):

    # проверям к какому посту привязан файл и есть ли права у данного пользователеля на доступ
    def get(self, request, *args, **kwargs):
        private_file = self.get_private_file()
        instance = ''

        if not self.can_access_file(private_file):
            raise PermissionDenied(self.permission_denied_message)
        else:
            creators_list = group_required_post(request)
            filename = self.get_content_disposition_filename(private_file)
            f1 = AttachmentsModel.objects.all()
            for fn in f1:
                f = AttachmentsModel.objects.filter(file=filename)
                for f2 in f:
                    f3 = f2.attachments_model.id
                    instance = Post.objects.filter(
                        id=f3,
                        post_groups__name__in=creators_list).distinct()

        if not private_file.exists():
            return self.serve_file_not_found(private_file)
        elif not instance:
            raise Http404("Файл не найден")
        else:
            return self.serve_file(private_file)
