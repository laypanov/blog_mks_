from django.contrib.auth.models import User, Group
from django.db import models
from django.db.models import Q
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor_uploader.fields import RichTextUploadingField
from private_storage.storage.files import PrivateFileSystemStorage
from taggit_selectize.managers import TaggableManager
from django.urls import reverse
from autoslug import AutoSlugField
from private_storage.fields import PrivateFileField
import random

DEFAULT_SINGLETON_INSTANCE_ID = 1


def slugify_function(content):
    for c in content:
        if c == '_':
            return content
    rand = random.randint(1, 10000)
    return content.lower() + '_' + str(rand)


class PostManager(models.Manager):
    use_for_related_fields = True

    def search(self, query=None):
        qs = self.get_queryset()
        if query:
            or_lookup = (Q(title__icontains=query) | Q(content__icontains=query) | Q(tags__name__icontains=query))
            qs = qs.filter(or_lookup)
        return qs


class General(models.Model):
    general_groups_admin = models.ForeignKey(Group, related_name='regulations_general_groups_admin',
                                             verbose_name='Администратор группы',
                                             on_delete=models.CASCADE, null=True)

    general_groups = models.ForeignKey(Group, related_name='regulations_general_groups',
                                       verbose_name='Разрешение для группы',
                                       on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = 'Группа доступа'
        verbose_name_plural = 'группа доступа'

    def __str__(self):
        return str(self.general_groups)


storage = PrivateFileSystemStorage(
    location='media/private-media/regulations/',
    base_url='regulations/private-media/regulations'
)


class AttachmentsModel(models.Model):
    file = PrivateFileField("Файл", storage=storage, null=True)
    date_edit = models.DateTimeField('Дата изменения', auto_now=True, null=True)
    attachments_model = models.ForeignKey('Post', null=True, blank=True, related_name='regulations_post_model',
                                          on_delete=models.CASCADE, verbose_name='Пост')

    def get_absolute_url(self):
        return "/regulations/private-media/regulations/"

    def __str__(self):
        return self.attachments_model.title

    class Meta:
        verbose_name = 'прикрепленный файл'
        verbose_name_plural = 'Прикрепленные файлы'


class Post(models.Model):
    title = models.CharField('Заголовок', max_length=150)
    category = TreeForeignKey('Category', null=True, blank=True, on_delete=models.CASCADE, verbose_name='Привязка к '
                                                                                                        'меню')
    content = RichTextUploadingField('Содержание')
    date_create = models.DateTimeField('Дата создания', auto_now_add=True)
    date_edit = models.DateTimeField('Дата изменения', auto_now=True)
    slug = AutoSlugField(slugify=slugify_function)
    published = models.BooleanField(default=True, verbose_name='Выводить в списке?')
    tags = TaggableManager(related_name='post_tags_regulations')
    post_groups = models.ManyToManyField(Group, related_name='post_groups_regulations',
                                         verbose_name='Разрешение для групп')
    objects = PostManager()

    def get_absolute_url(self):
        return '/regulations/category/%s/' % self.slug

    def get_absolute_url_tags(self):
        return '/regulations/tag/'

    class Meta:
        ordering = ('title',)
        verbose_name = 'информацию на странице'
        verbose_name_plural = 'Информация на страницах'

    def __str__(self):
        return self.title


class Category(MPTTModel):
    name = models.CharField(max_length=50, verbose_name='Название', unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children',
                            on_delete=models.CASCADE, verbose_name='Привязка к меню')
    slug = AutoSlugField()
    objects = PostManager()

    def get_bug_count(self):
        return Post.objects.filter(category=self).count()

    def get_absolute_url(self):
        return reverse('regulations', kwargs={'path': self.get_path()})

    def get_slug_list(self):
        try:
            ancestors = self.get_ancestors(include_self=True)
        except:
            ancestors = []
        else:
            ancestors_slug = [i.slug for i in ancestors]
            ancestors_name = [i.name for i in ancestors]
            ancestors = [i.slug for i in ancestors]

        c = []
        c2 = []
        c3 = ['|']

        for i in range(len(ancestors)):
            c.append('/'.join(ancestors_slug[:i + 1]))
            c2.append('/'.join(ancestors_name[:i + 1]))
            c[i] = c[i] + c3[0] + c2[i]
        return c

    class Meta:
        ordering = ('tree_id', 'lft')
        verbose_name = 'дополнительное меню'
        verbose_name_plural = 'Дополнительное меню'
        unique_together = ('parent', 'slug',)

    def __str__(self):
        return self.name
