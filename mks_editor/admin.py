from .models import General, ClientsMail
from django.contrib import admin
from solo.admin import SingletonModelAdmin

admin.site.register(
    General,
    SingletonModelAdmin,
    fields=[
        'general_groups_admin',
        'general_groups',
    ]
)

admin.site.register(
    ClientsMail,
    fieldsets=(
        ('', {
            'fields': (
                'employ_id',
                'member_name',
                'reg_nomer',
                'email',
                'status',
                'date_send',
            )
        }),
    ),
    list_display=[
        'employ_id',
        'member_name',
        'reg_nomer',
        'email',
        'status',
        'date_send',
    ],
    readonly_fields=['date_send']
)
