import json
import smtplib
import requests as requests

from datetime import datetime
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import basename

from django.http import HttpResponse
from django.shortcuts import render
from blog_mks.sql import csql
from users_info.models import UsersExtAD, UsersExtLynxEmploys
from .conn_pass import mail_user, mail_password
from .models import ClientsMail
from .sql_data import get_clients_adv
from .utils import group_required, check_perms_admin

url_msk_mks = 'https://url/reminder/'


@group_required()
def mks_editor(request):
    cursor = csql.sqlconnect(database='')
    ldb_visor = request.user.extlynxdb.ldb_visor
    object_list = []
    clients_mail = []

    if check_perms_admin(request.user):
        ldb_visor = "'%'"

    data = csql.sqlexec(request, cursor, "exec MKS_Editor_Get_List "
                                         "@ldb_visor = "'%s'";""" % ldb_visor)

    for row in data.fetchall():
        object_list.append(row)
        clients_mail = ClientsMail.objects.all()

    return render(request, 'mks_editor/list.html', {
        # 'today': now().date()
        'object_list': object_list,
        'clients_mail': clients_mail,
    })


@group_required()
def mks_editor_new(request):
    return render(request, 'mks_editor/create_new.html', {
    })


@group_required()
def mks_search2_ajax(request):
    ldb_visor = request.user.extlynxdb.ldb_visor

    # юзер из ростова видит и ростов и склад
    if ldb_visor == 0 or ldb_visor == 1:
        ldb_visor = "(Branch_ID='0' or Branch_ID='1')"

    # админ видит все
    if check_perms_admin(request.user):
        ldb_visor = "Branch_ID >= '0'"

    if isinstance(ldb_visor, int):
        ldb_visor = "Branch_ID =" + str(ldb_visor)
    data2 = []

    if request.is_ajax and request.method == "POST":
        post_data = request.POST.get('post_data')
        cursor = csql.sqlconnect(request)
        data = csql.sqlexec(request, cursor, get_clients_adv(post_data, ldb_visor))

        for row in data.fetchall():
            data2.append({"id": row.Client_id[2:], "text": row.Member_name + " [" + row.Client_id + "]",
                          "Member_name": row.Member_name, "Net_ID": row.Net_ID, "Net_name": row.Net_name,
                          "Client_id": row.Client_id, "contact": row.contact, "block": row.Block, "mail": row.Mail,
                          "Limit_Rez": row.Limit_Rez})
        return HttpResponse(json.dumps(data2), content_type="application/json;charset=utf-8")
    return None


@group_required()
def get_clrezerv(request):
    if request.is_ajax and request.method == "POST":
        client_id = request.POST.get('data')
        sql = """Exec [ClientLimitRezervLst] '%s'""" % client_id
        cursor = csql.sqlconnect(request)
        data = csql.sqlexec(request, cursor, sql)
        clrezerv = data.fetchone()
        clrezerv = str(clrezerv[0]).split('.')
        clrezerv = clrezerv[0]
        if clrezerv:
            return HttpResponse(json.dumps("%s, %s" % (client_id, clrezerv)),
                                content_type="application/json;charset=utf-8")
        else:
            return HttpResponse(json.dumps(""),
                                content_type="application/json;charset=utf-8")


def get_clrezervcheck(request):
    if request.is_ajax and request.method == "GET":
        sql = """select RegNomer, employ_id
            from MKS_Editor_CMP_ClientIns_Adv"""

        cursor = csql.sqlconnect()
        data = csql.sqlexec(request, cursor, sql)
        object_list = data.fetchall()

        for ol in object_list:
            if ol.employ_id is not None:

                sql2 = """MKS_Editor_Edit_Get @RegNomer = '%s'""" % ol.RegNomer
                cursor2 = csql.sqlconnect()
                data2 = csql.sqlexec(request, cursor2, sql2)
                object_list2 = data2.fetchall()

                for ol2 in object_list2:
                    sql3 = """Exec [ClientLimitRezervLst] '%s'""" % ol2.Client_id
                    cursor3 = csql.sqlconnect()
                    data3 = csql.sqlexec(request, cursor3, sql3)
                    object_list3 = data3.fetchall()

                    for ol3 in object_list3:
                        if ol3.SumSchet > ol3.Limit_Rez:
                            us = UsersExtAD.objects.get(employ_id__employ_id=ol.employ_id)

                            creator_email = ''
                            try:
                                msg = MIMEMultipart()
                                server = smtplib.SMTP('')
                                toaddr = '%s' % us.email  # mails
                                cc = '%s' % creator_email
                                reply = '%s' % creator_email
                                fromaddr = ''

                                rcpt = [toaddr] + [cc] + [reply] + [fromaddr]
                                msg['Subject'] = 'Недостаточно лимита у клиента Web-МКС [%s]' % ol2.Member_name
                                msg['to'] = toaddr
                                msg['cc'] = cc
                                msg['reply-to'] = reply

                                body = "<p>Сумма счетов: {1}</p>" \
                                       "<p>Установленный лимит: {2} </p>" \
                                       "<p><a href=http://url/mks_editor/details/{0}/>Необходимо изменить</p>".format(
                                    ol.RegNomer, ol3.SumSchet, ol3.Limit_Rez)
                                msg.attach(MIMEText(body, 'html', _charset='utf-8'))

                                text = msg.as_string()
                                server.sendmail(fromaddr, rcpt, text)
                            except:
                                pass


@group_required()
def mks_reg_create(request):
    if request.is_ajax and request.method == "POST":
        data_added = request.POST.get('data_added')
        member_name = request.POST.get('member_name')
        email = request.POST.get('email')
        pathname = request.POST.get('pathname').split('/')
        data_rem = request.POST.get('data_rem')
        reg_nomer = '0'
        ldb_visor = request.user.extlynxdb.ldb_visor
        employ_id = request.user.extlynxdb.ldb_employid
        cursor = csql.sqlconnect(request)

        if pathname[2] == "new":
            create_cu = """EXEC MKS_Editor_CMP_UsersIns 
                            @mail = '%s',
                            @username = '%s',
                            @ldb_visor = '%s',
                            @employ_id = '%s';""" % (email, member_name, ldb_visor, employ_id)

            data = csql.sqlexec(request, cursor, create_cu)
            reg_nomer = data.fetchone()
            reg_nomer = reg_nomer[0]
            ClientsMail.objects.create(reg_nomer=reg_nomer, email=email, employ_id=employ_id, member_name=member_name)

        elif pathname[2] == "details":
            reg_nomer = pathname[3]
            create_cu = """GCMP_UsersIns 
                            @Baza_id = '0R',
                            @RegNomer = '%s',
                            @UserName = '%s',
                            @Mail = '%s',
                            @OldRegNomer = '%s';""" % (reg_nomer, member_name, email, reg_nomer)

            csql.sqlexec(request, cursor, create_cu)

            try:
                cm = ClientsMail.objects.get(reg_nomer=reg_nomer)
                if cm.email != email:
                    ClientsMail.objects.filter(reg_nomer=reg_nomer).update(email=email, status=None)
                if cm.member_name != member_name:
                    ClientsMail.objects.filter(reg_nomer=reg_nomer).update(member_name=member_name)
            except:
                ClientsMail.objects.create(reg_nomer=reg_nomer, email=email, employ_id=employ_id,
                                           member_name=member_name)

        if data_added:
            json_string = json.loads(data_added)

            for js in json_string:
                limit_rez = int(js['Limit_Rez'])
                limit_rez_old = int(js['Limit_Rez_Old'])
                client_id = js['Client_id']

                create_ci = """
                            EXEC CMP_ClientIns 
                            @Baza_id = N'0R',
                            @RegNomer = N'%s',
                            @Client_Id = N'%s';""" % (reg_nomer, client_id)
                csql.sqlexec(request, cursor, create_ci)
                if limit_rez != limit_rez_old:
                    create_lr = """
                                update Clients_Net
                                set  Limit_Rez = '%s'
                                where Rec_Id = (select Rec_Id from Clients_Net  
                                where Rec_Id = (
                                select Net_ID from ViewClients
                                where Client_id = '%s')) """ % (limit_rez, client_id)
                    csql.sqlexec(request, cursor, create_lr)

        if data_rem:
            json_string_rem = json.loads(data_rem)
            for js in json_string_rem:
                client_id = js['Client_id']
                client_del = """Exec CMP_ClientDel '0R','%s','%s'""" % (reg_nomer, client_id)

                cursor = csql.sqlconnect(request)
                csql.sqlexec(request, cursor, client_del)

        return HttpResponse(json.dumps("done, %s" % reg_nomer),
                            content_type="application/json;charset=utf-8")


@group_required()
def details(request, slug):
    # slug - проверить
    cursor = csql.sqlconnect(request)
    get_ec = """MKS_Editor_Edit_Get @RegNomer = '%s'""" % slug
    data = csql.sqlexec(request, cursor, get_ec)

    object_list = []

    for row in data.fetchall():
        object_list.append(row)

    get_oc = """select RegNomer, UserName, Mail
        from CMP_Users
        where RegNomer = '%s'""" % slug
    data_oc = csql.sqlexec(request, cursor, get_oc)
    object_list2 = data_oc.fetchone()

    return render(request, 'mks_editor/details.html', {
        'object_list': object_list,
        'object_list2': object_list2,
    })


def send_email_regdata_day(request):
    data = ClientsMail.objects.filter(status=None)
    mail(data)


def mail(mails):
    EMAIL_HOST_USER = mail_user
    EMAIL_HOST_PASSWORD = mail_password

    for us in mails:
        employ_id = us.employ_id
        if employ_id:
            Employ = UsersExtAD.objects.get(employ_id__employ_id=employ_id)
            creator_email = Employ.email
        else:
            creator_email = ''
        try:
            msg = MIMEMultipart()
            server = smtplib.SMTP('')
            toaddr = '%s' % us.email  # mails
            cc = '%s' % creator_email
            reply = '%s' % creator_email
            fromaddr = ''

            rcpt = [toaddr] + [cc] + [reply] + [fromaddr]
            msg['Subject'] = 'Регистрационные данные Web-МКС [%s]' % us.member_name
            msg['to'] = toaddr
            msg['cc'] = cc
            msg['reply-to'] = reply

            body = "<p>Добрый день</p>" \
                   "<p>Логин и пароль для первого входа одинаковые: <b>{0}</b></p>" \
                   "В течение суток доступ будет предоставлен</p>" \
                   "<p>Информационное посьмо во вложении</p>".format(us.reg_nomer, us.reg_nomer)
            msg.attach(MIMEText(body, 'html', _charset='utf-8'))

            files = ['static/mks_editor/inf.docx']
            print(files)
            for f in files or []:
                with open(f, "rb") as fil:
                    part = MIMEApplication(
                        fil.read(),
                        Name=basename(f)
                    )

                part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
                msg.attach(part)

            text = msg.as_string()
            server.sendmail(fromaddr, rcpt, text)
        except:
            ClientsMail.objects.filter(reg_nomer=us.reg_nomer).update(status=False)
        ClientsMail.objects.filter(reg_nomer=us.reg_nomer).update(status=True, date_send=datetime.now())


def send_email_again(request):
    if request.is_ajax and request.method == "POST":
        reg_nomer = request.POST.get('reg_nomer')
        ClientsMail.objects.filter(reg_nomer=reg_nomer).update(status=None)
    return HttpResponse(json.dumps("done"),
                        content_type="application/json;charset=utf-8")


def send_email_restorekey(request):
    if request.is_ajax and request.method == "POST":
        reg_nomer = request.POST.get('reg_nomer')

        r = requests.post(url_msk_mks,
                          data={'email': reg_nomer},
                          cookies=dict(b2bcompocontragent='5534456', compodeliverydean='1', compokm='0',
                                       showDemoLoginDialog='True', )
                          )
        return HttpResponse(json.dumps("%s" % r),
                            content_type="application/json;charset=utf-8")
