# CREATE PROCEDURE [dbo].[MKS_Editor_Get_List]
"""
USE [Lynx_DB];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE PROCEDURE [dbo].[MKS_Editor_Get_List]
WITH EXEC AS CALLER
AS
create table  #MKS_Editor_Get
(
Baza_id varchar(255),
RegNomer varchar(255),
UserName varchar(255),
Status varchar(2),
DataUpd datetime,
UserUpd varchar(255),
Mail varchar(255),
Status_Nm varchar(255),
)

create table  #MKS_Editor_Get2
(
RegNomer varchar(255),
Row_Count varchar(255),

)
insert into #MKS_Editor_Get
exec CMP_UsersLst '0R'

insert into #MKS_Editor_Get2 (RegNomer, Row_Count)
(select  RegNomer, COUNT(Baza_id) as Row_Count1 from [CMP_Client]
where RegNomer IN (select RegNomer from #MKS_Editor_Get) group by RegNomer)

select #MKS_Editor_Get.RegNomer, UserName, Status, DataUpd, UserUpd, Mail, Status_Nm, Row_Count
from #MKS_Editor_Get left join #MKS_Editor_Get2
ON #MKS_Editor_Get.RegNomer = #MKS_Editor_Get2.RegNomer

GO
"""


def get_clients_adv(post_data, ldb_visor):
    return """
        SELECT
        LVC.Client_id, LVC.Net_ID, LVC.Net_name, LVC.Member_name, LVC.contact, LVC.Block,
        (SELECT Mail
        FROM CLIENTS AS CLIENTS
        WHERE CLIENTS.CLIENT_ID = LVC.Client_id)
        AS
        Mail,

        (SELECT Limit_Rez
        FROM Clients_Net AS Clients_Net
        WHERE Clients_Net.Rec_Id = LVC.Net_ID)
        AS
        Limit_Rez
        FROMViewClients
        AS
        LVC
        where (Client_id LIKE '%%%s%%' or Member_name LIKE '%%%s%%' or contact LIKE '%%%s%%') and (%s) and 
        Client_type IN (0, 1, 2) and contact != 'НЕРАБОТАЮЩИЙ КЛИЕНТ'""" % (post_data, post_data, post_data, ldb_visor)
