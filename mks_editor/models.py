from django.contrib.auth.models import Group, User
from django.db import models


class General(models.Model):
    general_groups_admin = models.ForeignKey(Group, related_name='mks_editor_general_groups_admin',
                                             verbose_name='Администратор группы',
                                             on_delete=models.CASCADE, null=True)

    general_groups = models.ForeignKey(Group, related_name='mks_editor_general_groups',
                                       verbose_name='Разрешение для группы',
                                       on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = 'Группа доступа'
        verbose_name_plural = 'группа доступа'

    def __str__(self):
        return str(self.general_groups)


class ClientsMail(models.Model):
    reg_nomer = models.CharField(verbose_name='Регистрационный номер', max_length=10)
    email = models.CharField(verbose_name='Почтовый адрес клиента', max_length=30)
    date_send = models.DateTimeField(verbose_name='Время отправки', null=True)
    status = models.BooleanField(verbose_name='Статус', null=True)
    employ_id = models.IntegerField(verbose_name='employ_id_creator', null=True)
    member_name = models.CharField(verbose_name='Имя пользователя', max_length=50, null=True)

    # null - будем отправлять
    # 0 - не отправляем
    # 1 - отправлено

    class Meta:
        verbose_name = 'рассылку писем'
        verbose_name_plural = 'Рассылку писем'

    def __str__(self):
        return str(self.reg_nomer)
