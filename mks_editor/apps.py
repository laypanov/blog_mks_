from django.apps import AppConfig


class MksEditorConfig(AppConfig):
    name = 'mks_editor'
    verbose_name = "Редактор МКС"
