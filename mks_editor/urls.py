from django.conf.urls import include, url
from . import views
from mks_data import registrations
from django.conf import settings
from django.urls import path

from .views import mks_search2_ajax, mks_reg_create, send_email_again, send_email_regdata_day, get_clrezerv, \
    send_email_restorekey, get_clrezervcheck

app_name = 'mks_editor'

urlpatterns = [
    path(r'', views.mks_editor, name='mks_editor'),

    path('post/mks_search2_ajax', mks_search2_ajax,
         name="mks_search2_ajax"),
    path('post/mks_reg_create', mks_reg_create,
         name="mks_reg_create"),
    path('post/send_email_again', send_email_again,
         name="send_email_again"),
    path('post/send_email_restorekey', send_email_restorekey,
         name="send_email_restorekey"),
    path('post/send_email_regdata_day', send_email_regdata_day,
         name="send_email_regdata_day"),
    path('post/get_clrezerv', get_clrezerv,
         name="get_clrezerv"),
    path('get/get_clrezervcheck', get_clrezervcheck,
         name="get_clrezervcheck"),
]
